#!/bin/sh

if [ $# -ne 1 ]; then
echo "You must provide the  file to be used"
echo "Use: sudo ./programpic.sh nameoffile.hex"
exit 1
fi
 
python burnLVP.py $1

