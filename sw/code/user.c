/*
 * File:   user.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 26 de febrero de 2013, 9:51
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * User routines to init and configure PIC
 *
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#define USE_OR_MASKS

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#endif

#include "user.h"
#include "serial.h"
#include "queue.h"
#include "timer.h"
#include "program.h"
#include "syst_config.h"

//for the delay
#include "system.h"        /* System funct/params, like osc/peripheral config */


/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void InitApp(void) {
    // Setup analog functionality and port direction
    InitPort();

    // Initialize peripherals
    InitUSART();
    InitTIMER();
    // Enable interrupts
    InitInt();
    //Init Variables
    InitVar();
    
    __delay_ms(500);

}

void InitPort(void) {

    PORTA = 0;
    //TRISA = TRISAconf;
    TRISA = TRISAout;
    
    PORTC = 0;
    TRISC = TRISCconf;

    ANSELA=ANSELAconf;
    ANSELC=ANSELCconf;
 #if defined(__16F1825)
    APFCON0=APFCON0conf;
#endif
#if defined(__16F18326)
    RXPPS=RXPPSconf;
    TXPPS=TXPPSconf;
    RC4PPS=RC4PPSconf;
    PMD0=PMD0conf;
    PMD1=PMD1conf;
    PMD2=PMD2conf;
    PMD3=PMD3conf;
    PMD4=PMD4conf;
    PMD5=PMD5conf;
#endif

}

void InitUSART() {
    CloseUSART();
    baudUSART(BAUDCONFIG);
    OpenUSART(CONFIGUSART, SPBRGconf);
}

void InitTIMER(void) {
    CloseTimer0();

#ifdef __16F1825
    OPTION_REG = OPTION_REGconf;
#endif
#ifdef __16F18326
    //OPTION_REG = OPTION_REGconf;
    T0CON0=T0CON0conf;
    T0CON1=T0CON1conf;

#endif

}

void InitInt(void) {

    INTCON = 0;
    PEIE = 1;    //periherials int (bit witin INTCON)
#ifdef __16F18326
    PIE0 = 0;
#endif
    TMR0IE=0;  //timer0 in pic16f1825 is on INTCON in 16f18346 in PIE1

    PIE1 = 0;
    RCIE = 1; //so far interrupt when receiving something
    
    //Rest of interruptions disconnected
    PIE2=0;
    PIE3=0;
#ifdef __16F18326
    PIE4 = 0;
#endif

    ei();
}

void InitVar(void) {
    abortsignal = false;
    commandlength = 0;
    bytesreceived = 0;
    status = CYCLEOFF;
    
    //__EEPROM_DATA(0x00,0x02,0xB5,0xBD,0x00,0x06,0x00,0x00);
/*    eeprom_write(0x00,0x00);
    eeprom_write(0x01,0x02);
    eeprom_write(0x02,0xB5);
    eeprom_write(0x03,0xBD);
    eeprom_write(0x04,0x00);
    eeprom_write(0x05,0x06);
  */  
    for (char i=0;i<4;i++){
    ZENTRAL[i]=eeprom_read(AZENTRAL+i);
    }
    
    for (char i=0;i<2;i++){
    NSESSION[i]=eeprom_read(ANSESSION+i);
    }
   
    initQueue();
}
/*
void InitTrack(void){
    
        SignalTrack1(); //For MMX...not sure if needed... not sure if it affects therprotocols
       // TRISA =TRISAout;
}

void IddleTrack(void){
   // PORTA=0;
   // TRISA = TRISAconf;
}
*/
void SignalTrack0(void){
      //  PinSignalR=0;
      //  PinSignalB=1;
/*TESTING. IT SEEMS THAT CHANGING PORTA PINS THOUGH RA ON CONSEQUITIVE LINES... GLTICHES.
 * PROBABLY BECAUSE SECOND COMMANDS READS FROM PORTA AND LATA HAS NOT REACH YET TO PORT
 *         for (char i=0;i<10;i++){
        NOP();
        }
        PinMMXout=1;
*/
    //Porta only output affected
     if (!abortsignal) {
    PORTA=0b00010000;
     }
 }
void SignalTrack1(void){
        //PinSignalR=1;
       // PinSignalB=0;
/*        for (char i=0;i<10;i++){
        NOP();
        }        
        PinMMXout=0;
 */
    //Porta only output affected
     if (!abortsignal) {
    PORTA=0b00100000;
     }
}
void SignalTrackSW(void){
    //PinSignalR=~PinSignalR;
    //PinSignalB=~PinSignalB;
    //Porta only output affected
     if (!abortsignal) {        //Necessary because in mmx, even without transmitting bit (abort signal in bit0 and 1mmx, there are changes od Signalrack and delays)
    PORTA=~PORTA;
     }
}