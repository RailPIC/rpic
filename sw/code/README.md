# RAILPIC PROJECT #

|                       |                                                      |
|:----------------------|:-----------------------------------------------------|
|**Project Owner(s):**  |Manuel Serrano                                        |
|**Author(s):**         |Manuel Serrano                                        |
|**Hardware Platform:** |ODROID, RASPBERRY PI, PIC 16f18326, PIC16f1825        |
|**Debuggers Used:**    |GCC, ECLIPSE, MPLAB Simulator                         |
|**Programmers Used:**  |PICKIT3, PICKIT2                                      |
|**MPLAB Version:**     |>1.70                                                 |
|**C Compiler Version:**|1.12<xc8<2.0 gcc>4.8.4                                |

**For more information:** [RailPic Homepage](http://tren.enmicasa.net/railpic)  

********************************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

********************************************************************************

RAILPIC is a project to embed the control of model railways in a PIC ucontroller
connected to an arm device..

Currently the project provides:

*   Support for MM1 and MM2 protocol
*	Support for NMRA (long and short addresses) – 14,28 and 128 steps
*	Support for MMX (MM4) (not yet reading responses, but with the UID of 
the loco, it is possible to register the loco and send commands to it.)
*	Support for NMRA accessories (no extended accessories so far)
*	Support for writing CV.

The definition of the MM protocol was obtained from:

- ["The Manual of the new Märklin Motorola Format"](http://spazioinwind.libero.it/scorzoni/motorola.htm), 
Andrea Scorzoni, 1995-2000.  

The definition of the MFX protocol was obtained from:
- ["Das mfx-Schienenformat"](http://www.skrauss.de/modellbahn/mdigital.html),
Stefan Krauß, Version 1 2009-02-01.  
- [Rainer Mueller web site](http://www.alice-dsl.net/mue473/mfxrahmen.html).

## FILES INCLUDED IN THE PROJECT TREE ##

system.h - Contains custom oscillator configuration function prototypes,
reset source evaluation function prototypes, and non-peripheral
microcontroller initialization function prototypes.  It also may contain
system level #define macros and variables.  This file is included
by system.c

user.h - Contains parameters and function prototypes used in user.c for user
level functions, for example, InitApp();

configuration_bits.c - Contains device configuration bit macros.  Refer to
the comments in configuration_bits.c for instructions on where to get
information about device configuration bits and their corresponding macros.

interrupts.c - This file contains example stubs for interrupts.  The user would
put the interrupt vectors for their application in interrupts.c.

main.c - This is the main code for the project.  global variables and the
main() function are located in main.c  The user would put their primary program
flow in main.c, and the operation should be contained in main.c with an
infinite loop.

system.c - Contains custom oscillator configuration functions, reset source
evaluation functions, and non-peripheral microcontroller initialization
functions.  Functions in system.c would often be called from main.c during
device initialization.

user.c - Custom user algorithms, user peripheral initialization, data
computation functions, and calculation functions would go here.  Prototypes for
user.c go in user.h.

mm.c - MM routines to generate the signal
mmx.c - MFX routines to generate the signal
nmra.c - NMRA routines to generate the signal
program.c - Routines to program decoders
queue.c - Routines to manage the queue where messages are stored
serial.c - Routines to manage the serial port
timer.c - routines to manage a timeout

FILES INCLUDED IN THE PROJECT BUT NOT IN THE PROJECT TREE:

#include <htc.h>             /* Global Header File */
#include <stdint.h>          /* For uint8_t definition */
#include <stdbool.h>         /* For true/false definition */

These files come with the HiTech compiler.  Check the compiler documentation for
more information on these files.
