/*
 * File:   mm.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MM Routines
 *
 * The definition of the protocol was obtained from:
 * "The Manual of the new M�rklin Motorola Format"
 * Andrea Scorzoni, 1995-2000
 * http://spazioinwind.libero.it/scorzoni/motorola.htm
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "mm.h"
#include "user.h"           //definition on bit and byte operation
#include "system.h"         //definition on clock for timing
#include "queue.h"          //queue is here
#include "serial.h"         //buffer is here and also abortsignal
#include "program.h"         //PTactive
#include "timer.h"         //CCP3CONconf

/******************************************************************************/
/* MM Functions                                                             */
/******************************************************************************/

char locoMM() {
    char result;                        //for future control error
    char npairs = 2;
    char i;

    result = getMMaddress();
    if (result!=OK){
        return result;
    }

    
    //So far no other mode rady
    if (buffer[POSNEWTYPE] == LOCOMM2Fx) {
        result = getMMFx();
    } else {
        result = getMMspeed();
    }

    if (status == NEWCMD) {
        if (buffer[POSNEWTYPE] == LOCOMM2Fx) {
            npairs = NFXPAIRS;
        } else {
            npairs = NSPEEDPAIRS;
        }
        //LOCOMM1R does not exist anymore as packet coming from rocrail.
        //Thus, when a LOCOM1 arrives it has the absolute direction...
        //and it has to be updated.. but as MM1 - treated in updatequee
 
            //test.. in principle locoMM is updated when reading oldSpeed
            // int posToWrite=findatQueue(QUEUE_MM);
           // char result=updateQueue(posToWrite);
           // if (result!=OK) return result;
        
  
  //      if (!PTidle) updateQueueMM();
    }
   
        
    for (i = 0; i < npairs; i++) {

       // InitTrack();
        //double packet
        sendMMbyte(bytesMM[0]);
        sendMMF0();
        sendMMbyte(bytesMM[1]);

      //  IddleTrack();
        __delay_us(TIMEMMBTW);
    
      //  InitTrack();
        sendMMbyte(bytesMM[0]);
        sendMMF0();
        sendMMbyte(bytesMM[1]);

      //  IddleTrack();
       __delay_us(TIMEMMAFT);
    }

    return OK;
}

char getMMaddress() {
    char address;                    //format A1A2A3A4  applies to any MM type
    char mod;
    char i;

    bytesMM[0] = 0;

    //so far both MM1 and MM2 share the same address coding
    address = buffer[1];
    if ((address<1)|(address>80)){
        return ERROR10;
        }
    
    if (address != 80) {                            //adress==80 is 0000
        for (i = 0; i < 8; i = i + 2) {             //prepare first byte
            mod = address % 3;                      //natural way of decoding... if mod=0..code 00
            if (mod != 0) {                         //if mod!=0... code 11 or 10
                setbit(bytesMM[0], 7 - i);
                if (mod == 1) {                     //if mode==1  code 11
                    setbit(bytesMM[0], 7 - i - 1);
                }
            }                                       //if no if...code 00 was alreayd there
            address = address / 3;
        }
    }
    return OK; 
}

char getMMspeed() {
    char speed;                     //format S1S2S3S4
    char opmode;                    //applies to MM2 type
    char i;

    speed = buffer[2]&0b01111111;   //remove the bit of direction
    bytesMM[1] = 0;                 //if speed=0.. nothing else to do
    
    //no control speed 0-14 so far
    if (speed > 0) {
        speed = speed + 1;          //speed 1 estarts in 2 and 14 is 15
    }
    switch (buffer[POSNEWTYPE]) {
        case LOCOMM1R:
            speed = 1;              //special reverse packet
        case LOCOMM1:
            for (i = 0; i < 4; i++) {
                if (testbit(speed, i)) {             //speed is DCBA
                    setbit(bytesMM[1], 7 - i * 2);   //byte AABBCCDD
                    setbit(bytesMM[1], 6 - i * 2);   //byte AABBCCDD
                }
            }
            break;
        case LOCOMM2:
            if testbit(buffer[2], 7) {  //forward
                if (speed > 7) {
                    opmode = 0b0100;
                } else {
                    opmode = 0b0101;
                }       //Note speed is already +1.. so speed>7.. means opmode+1>7=opmode>6
            } else {                    //reverse
                if (speed > 7) {
                    opmode = 0b1010;
                } else {
                    opmode = 0b1011;
                }
            }

            for (i = 0; i < 4; i++) {
                if (testbit(speed, i)) {            //speed is DCBA
                    setbit(bytesMM[1], 7 - i * 2);  //byte AEBFCGDH... speed in ABCD
                }
                if (testbit(opmode, 3-i)) {
                    setbit(bytesMM[1], 6 - i * 2);  //byte AEBFCGDH... opmde in EFGH
                }
            }
            break;
        default:
            return 0;
                    //nothing done yet... but should be a return error
    }
    return OK;      //so far no control error
}

char getMMFx() {
    char speed;     //format S1S2S3S4
    char opmode;    //applies to MMFx protocols...so far not implemented the MM1Fx
    char i;

    //FX modes, also requires the speed information too
    speed = buffer[2]&0b01111111;   //remove the bit of direction
    bytesMM[1] = 0; 

    //no control speed 0-14
    if (speed > 0) { 
        speed = speed + 1;          //speed 1 estarts in 1 and 14 is 15
    }

    //So far only MM2Fx.. but I would assume all o them are the same
    switch (buffer[POSNEWFGROUP]) {
        case 1:
            opmode = 0b1100;
            if (testbit(buffer[3], 1)) {
                setbit(opmode, 0);
            }
            //exceptions table 4b 
            if (((speed - 1) == 2)&(!(testbit(buffer[3], 1)))) {
                clrbit(opmode, 2);
            }
            if (((speed - 1) == 10)&((testbit(buffer[3], 1)))) {
                clrbit(opmode, 3);
            }
            break;
        case 2:
            opmode = 0b0010;
            if (testbit(buffer[3], 2)) {
                setbit(opmode, 0);
            }
            //exceptions table 4b 
            if (((speed - 1) == 3)&(!(testbit(buffer[3], 2)))) {
                setbit(opmode, 3);
            }
            if (((speed - 1) == 11)&((testbit(buffer[3], 2)))) {
                setbit(opmode, 2);
                clrbit(opmode, 1);
            }
            break;
        case 3:
            opmode = 0b0110;
            if (testbit(buffer[3], 3)) {
                setbit(opmode, 0);
            }
            //exceptions table 4b 
            if (((speed - 1) == 5)&(!(testbit(buffer[3], 3)))) {
                setbit(opmode, 3);
                clrbit(opmode, 2);
            }
            if (((speed - 1) == 13)&((testbit(buffer[3], 3)))) {
                clrbit(opmode, 1);
            }
            break;
        case 4:
            opmode = 0b1110;
            if (testbit(buffer[3], 4)) {
                setbit(opmode, 0);
            }
            //exceptions table 4b 
            if (((speed - 1) == 6)&(!(testbit(buffer[3], 4)))) {
                clrbit(opmode, 2);
            }
            if (((speed - 1) == 14)&((testbit(buffer[3], 4)))) {
                clrbit(opmode, 3);
                clrbit(opmode, 1);
            }
            break;
        default:
            return 0; //so far no error control
    }
    for (i = 0; i < 4; i++) {
        if (testbit(speed, i)) {
            setbit(bytesMM[1], 7 - i * 2); //byte AEBFCGDH... speed in ABCD
        }
        if (testbit(opmode, 3-i)) {
            setbit(bytesMM[1], 6 - i * 2); //byte AEBFCGDH... fx in EFGH
        }
    }
    return OK; //so far no control error
}

void sendMMbyte(char byte) {
    char i;
    for (i = 0; i != 8; i++) {
        if (testbit(byte, 7 - i)) {
            Bit1MM();
        } else {
            Bit0MM();
        }
    }
}

void sendMMF0() {
    //send F0 is a pecial packet.. since it is sent btween two bytes
    if (testbit(buffer[3], 0)) {
        Bit1MM();
        Bit1MM();
    } else {
        Bit0MM();
        Bit0MM();
    }
}
 
void Bit0MM(void) {
    if (!abortsignal) { //If we are on an interruption we do want to send the message


            //PinSignal = 0;
            SignalTrack0();
        
        __delay_us(TIMESHORTMMLOCL);

            //PinSignal = 1;    
            SignalTrack1();
        __delay_us(TIMELONGMMLOCH);
    }
}

void Bit1MM(void) {
    if (!abortsignal) { //If we are on an interruption we do want to send the message

           //PinSignal = 0;
            SignalTrack0();
        
        __delay_us(TIMELONGMMLOCL);

            //PinSignal = 1;
            SignalTrack1();
        __delay_us(TIMESHORTMMLOCH);
    }
}
