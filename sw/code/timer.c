/*
 * File:   timer.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 10 de marzo de 2013, 9:05
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Timers routines
 * 
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#define USE_OR_MASKS

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "timer.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void OpenTimer0( unsigned char config){
   TMR0IE=1;
   TMR0IF=0;     //ENABLE Interruption.. timer0 is always running in 16f1825
                 //no timer0 on off
#if defined(__16F18326)
   T0EN=1;
#endif        
          
}

void CloseTimer0(){
    TMR0IE=0;     //DISABLE Interruption.. timer0 is always running;
                  //no timer0 on off
#if defined(__16F18326)
   T0EN=0;
#endif 

}

void WriteTimer0(char data){
#if defined(__16F1825)
    TMR0=data;  //potentially need to add 1..256 extra instruction cycles...because timer0 is always on...so when enabling the interruption... timer will be discounted
                //no timer0 on off
#elif defined(__16F18326)
    TMR0H=data;
    TMR0L=0;
#endif
}


void startTimeout(){
    WriteTimer0(TIMERcount);
   OpenTimer0(CONFIGTIMER);
}
void stopTimeout(){
    CloseTimer0();
}

