/* 
 * File:   nmra.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 26 de febrero de 2013, 9:51
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  NMRA Definitions
 *
 * Timing adapted for the current routines and 32Mhz clock
 * Further timing improvements may lead to new timing definitions
 * Faster cloks also requires new timing definitions
 *
 * Standar timing should be:
 * SHORT: 58us
 * LONG: 100us
 * 
 */

#ifndef NMRA_H
#define	NMRA_H

#ifdef	__cplusplus
extern "C" {
#endif

#define NORMALpreambleBitsNMRA      14
#define LONGpreambleBitsNMRA        30

#define MAXBYTESNMRA                5

//#if _XTAL_FREQ>20000000
#define TIMEBTCYCLE           1   //Time in ms between cycle (as low as possible accroding to standard 30ms is hte max.
//#else                             //large clocks do not take the long wait ms
//#define TIMEBTCYCLE           10
//#endif

#define TIME1NMRAH             54  //Time in us for 1 level High;
#define TIME1NMRAL             57  //Time in us for 1 level Low;
#define TIME0NMRAH             98  //Time in us for  0 level High;
#define TIME0NMRAL             98  //Time in us for 0 level� Low ;

//Definition of command and type
#include "commands.h"

//it has to be extern becasue bytesNMRA is also used by the program routines
extern char bytesNMRA[MAXBYTESNMRA];

char locoNMRA(void);
char accNMRA(void);
char getNMRAaddress(void);
char getNMRAspeed(char startbyte);
char getNMRAfx(char startbyte);
char getNMRAacc(char startbyte);
void idlePacket(char preambleBitsNMRA);
void broadcastPacket(char speed);
void resetPacket(char preambleBitsNMRA);
void pagePacket(char preambleBitsNMRA);
void sendNMRApreamble(char preambleBitsNMRA);
void sendNMRAbyte(char byte);
void Bit0NMRA(void);
void Bit1NMRA(void);

#ifdef	__cplusplus
}
#endif

#endif	/* NMRA_H */


