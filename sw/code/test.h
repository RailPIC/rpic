/*
 * File:   test.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 10 de marzo de 2013, 9:05
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Test routines
 * 
 */

extern char ntest;
#ifndef TEST_H
#define	TEST_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif


void test(void);
#endif	/* TEST_H */

