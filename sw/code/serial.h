/* 
 * File:   serial.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 3 de marzo de 2013, 18:48
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serial port definitions
 */

#ifndef SERIAL_H
#define	SERIAL_H

#ifdef	__cplusplus
extern "C" {
#endif

//#define MAXBYTES    12  //actually, it seems 11 is the maximum I will send
#define MAXBYTES    22  //actually, it seems 11 is the maximum I will send... but coming frm queeu it will be 21 (maximum MFXfn)

#define CONFIGUSART         0       //everything is directly defined in the function. I am enabling it for compatibility with the pic18 code
#define BAUDCONFIG          0b01001000  //BRG16 on
//#define BAUD1CONFIG         0b01001000
#define SPBRGconf           68           //10 for 20 Mhz...
#define TXSTAconf           0b10100110   //BRGH an TX enable
//#define TX1STAconf          0b10100110   //BRGH an TX enable
#define RCSTAconf           0b10010000   //RX cont, enable POrt
//  #define RC1STAconf          0b10010000   //RX cont, enable POrt


extern char buffer[MAXBYTES];
extern char bufferIN[MAXBYTES];
extern char commandlength;
extern char bytesreceived;
extern bit abortsignal;
extern char status;
extern char old_status;


void OpenUSART( unsigned char config, unsigned int spbrg);
void CloseUSART();
void baudUSART (unsigned char baudconfig);
char ReadUSART(void);
void WriteUSART(char data);
char checksum(char toverify[], char firstbyte, char lastbyte);
void bufferFirstByte(void);
void bufferNextBytes(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SERIAL_H */
