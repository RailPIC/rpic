/*
 * File:   test.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 10 de marzo de 2013, 9:05
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Test routines
 * 
 */

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)
#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#include "mmx.h"           //MMX defintiions
#include "nmra.h"          //NMRA
#include "serial.h"        //Buffer is here and also abortsignal, status chars
#include "queue.h"        //Buffer is here and also abortsignal, status chars

//char ntest = 0;
//bit light=false;

void test(void) {

    locosMessageQueue.position[QUEUE_MMX]=EMPTYQUEUE;
    
    
    /*
    
    //Change for modification Correct init (not sure if still working rest of MFX)
    //InitTrack();

    char i;
    char length = 0;
    char init = 0;

    //TEST5
    //switch (ntest) {
    if (ntest==0){
//        case 0:
            //BIND

            //New address 2
            buffer[1] = 0x00;
            buffer[2] = 0x02;
            //deco UID 0x7DFA8DFC? (la que no es DXII) antigua direcci�n 3
            buffer[3] = 0x7D;
            buffer[4] = 0xFA;
            buffer[5] = 0x8D;
            buffer[6] = 0xFC;

            bind_nadr(length);
            length = length + BIND_LENGTH;
            crc8(0, length);
            length = length + CRC8_LENGTH;
            sendMMXCommand(0, length);
            EndFlag();
            ntest++;
  //          break;
//        case 1:
    }else if (ntest==1){
            //PING new ADDRESS
            buffer[1] = 0x00;
            buffer[2] = 0x02;
            //deco UID 0x7DFA8DFC? (la que no es DXII) antigua direcci�n 3
            buffer[3] = 0x7D;
            buffer[4] = 0xFA;
            buffer[5] = 0x8D;
            buffer[6] = 0xFC;
            length = ping_verify(length);
            crc8(0, length);
            length = length + CRC8_LENGTH;
            sendMMXCommand(0, length);
            SimpleResponseMMX();
            ntest++;
//            break;
//        case 2:
    }else{
           
            //A07:1 V3:0 F04:1                                                    ;9 bits+7bits+7 bits=23bits
            //1 0 A6 A5 A4 A3 A2 A1 A0
            //0 0 0 R V4 V5 V6
            //0 1 0 F3 F2 F1 F0
            //1 0 A6 A5 A4 A3 A2 A1 A0
            putbitMMX(0, true);
            for (i = 1; i != 8; i++) {
                putbitMMX(i, false);
            }
            putbitMMX(8, true);

            length = length + 9; //9

            //0 0 0 R V4 V5 V6
            for (i = 0; i != 7; i++) {
                putbitMMX(length + i, false);
            }
            length = length + 7; //16

            //0 1 0 F3 F2 F1 F0
            //F0=1
            putbitMMX(length, false);
            putbitMMX(length + 1, true);
            for (i = 2; i != 6; i++) {
                putbitMMX(length + i, false);
            }
            //putbitMMX(length+6,true);
            putbitMMX(length + 6, light);
            length = length + 7; //23

            crc8(init, length);
            //sendMMXCommand(length+8); 
            //length=0;
            length = length + 8; //31  ///THIS VALUE!!!

            init = length;
            // sendMMXCommand(length+8); 
            // length=0;

            //A07:2 V3:0 F04:0                                                    ;9 bits+7bits+7 bits=23bits
            //1 0 A6 A5 A4 A3 A2 A1 A0
            //0 0 0 R V4 V5 V6
            //0 1 0 F3 F2 F1 F0

            //1 0 A6 A5 A4 A3 A2 A1 A0
            putbitMMX(length, true);
            for (i = 1; i != 7; i++) {
                putbitMMX(length + i, false);
            }
            putbitMMX(length + 7, true);
            putbitMMX(length + 8, false);
            //putbitMMX(length + 8, true);
            length = length + 9;

            //0 0 0 R V4 V5 V6
            for (i = 0; i != 7; i++) {
                putbitMMX(length + i, false);
            }

            length = length + 7;

            //0 1 0 F3 F2 F1 F0
            putbitMMX(length, false);
            putbitMMX(length + 1, true);

            for (i = 2; i != 6; i++) {
                putbitMMX(length + i, false);
            }
            putbitMMX(length + 6, light);

            length = length + 7;

            crc8(init, length);
            length = length + 8; ///THIS VALUE!!!
            sendMMXCommand(0, 31);
            sendMMXCommand(31, length);
            
            /*
            for (i = 0; i != 4; i++) {
                FlagMMX();
            }
            // 
            EndFlag();
            light=~light;
            ntest = 0;
         //   break;
    }
*/
    
    /*    
    //TEST4
    //A07:1 V3:0 F04:1                                                    ;9 bits+7bits+7 bits=23bits
    //1 0 A6 A5 A4 A3 A2 A1 A0
    //0 0 0 R V4 V5 V6
    //0 1 0 F3 F2 F1 F0
  
    
    //1 0 A6 A5 A4 A3 A2 A1 A0
    putbitMMX(0,true);
    for (i=1;i!=8;i++){
    putbitMMX(i,false);
    }
    putbitMMX(8,true);

    length=length+9;  //9

    //0 0 0 R V4 V5 V6
    for (i=0;i!=7;i++){
    putbitMMX(length+i,false);
    }
    length=length+7;  //16
 
    //0 1 0 F3 F2 F1 F0
    //F0=1
    putbitMMX(length,false);
    putbitMMX(length+1,true);
for (i=2;i!=6;i++){
    putbitMMX(length+i,false);
    }
    //putbitMMX(length+6,true);
    putbitMMX(length+6,false);
    length=length+7;  //23
 
    crc8(init,length);
    //sendMMXCommand(length+8); 
    //length=0;
    length=length+8;   //31  ///THIS VALUE!!!
    
    init=length;
   // sendMMXCommand(length+8); 
   // length=0;
    
    //A07:3 V3:0 F04:1                                                    ;9 bits+7bits+7 bits=23bits
    //1 0 A6 A5 A4 A3 A2 A1 A0
    //0 0 0 R V4 V5 V6
    //0 1 0 F3 F2 F1 F0
      
    //1 0 A6 A5 A4 A3 A2 A1 A0
    putbitMMX(length,true);
    for (i=1;i!=7;i++){
    putbitMMX(length+i,false);
    }
    putbitMMX(length+7,true);
    putbitMMX(length+8,true);
    length=length+9;  

    //0 0 0 R V4 V5 V6
    for (i=0;i!=7;i++){
    putbitMMX(length+i,false);
    }
 
    length=length+7;  
    
   //0 1 0 F3 F2 F1 F0
    putbitMMX(length,false);
    putbitMMX(length+1,true);

    for (i=2;i!=6;i++){
    putbitMMX(length+i,false);
    }
    putbitMMX(length+6,true);

    length=length+7;  

    crc8(init,length);
    length=length+8;     ///THIS VALUE!!!
    sendMMXCommand(0,31);  
    sendMMXCommand(31,length);
    
    //ResponseMMX();
    
    for(i=0;i!=4;i++){
    FlagMMX();
    }
     */

    //TEST3
    /*    char i;
        char length=0;
        char init=0;

        //A07:1 V3:0 F04:0                                                    ;9 bits+7bits+7 bits=23bits
        //1 0 A6 A5 A4 A3 A2 A1 A0
        //0 0 0 R V4 V5 V6
        //0 1 0 F3 F2 F1 F0
  
    
        //1 0 A6 A5 A4 A3 A2 A1 A0
        putbitMMX(0,true);
        for (i=1;i!=8;i++){
        putbitMMX(i,false);
        }
        putbitMMX(8,true);

        length=length+9;  //9

        //0 0 0 R V4 V5 V6
        for (i=0;i!=7;i++){
        putbitMMX(length+i,false);
        }
        length=length+7;  //16
 
        //0 1 0 F3 F2 F1 F0
        putbitMMX(length,false);
        putbitMMX(length+1,true);
    for (i=2;i!=7;i++){
        putbitMMX(length+i,false);
        }
        length=length+7;  //23
 
        crc8(init,length);
        //sendMMXCommand(length+8); 
        //length=0;
        length=length+8;   //31  ///THIS VALUE!!!
        init=length;
        //A07:0 Bake von Zentrale 0x2045F, Opt 0x0   63 bits
        //Broadcast Adress 0  bits 1 0         A6  A5  A4  A3  A2  A1  A0
        broadcastAddress(length);
        length=length+9;   //40

        //Comando: 1 1 1 1 0 1
         for (i=0;i!=4;i++){
            putbitMMX(length+i,1);
        }
        putbitMMX(length+4,0);
        putbitMMX(length+5,1);
    
        length=length+6;    //46

        buffer[1]=ZENTRAL3;
        buffer[2]=ZENTRAL2;
        buffer[3]=ZENTRAL1;
        buffer[4]=ZENTRAL0;
        //UUI Zentrale (32 bits))
        deco_zentral_uid(length);

        length=length+32; //78

        //Z Session (16 bits))=0
        for (i=0;i!=16;i++){
                putbitMMX(length+i,0);         
        }
        
        length=length+16; //94

        crc8(init,length);
         length=length+8;   //102  ///THIS VALUE!!!
         init=length;
       // sendMMXCommand(length+8); 
       // length=0;
    
        //A07:1 V3:0 F04:0                                                    ;9 bits+7bits+7 bits=23bits
        //1 0 A6 A5 A4 A3 A2 A1 A0
        //0 0 0 R V4 V5 V6
        //0 1 0 F3 F2 F1 F0
  
    
        //1 0 A6 A5 A4 A3 A2 A1 A0
        putbitMMX(length,true);
        for (i=1;i!=8;i++){
        putbitMMX(length+i,false);
        }
        putbitMMX(length+8,true);
        length=length+9;  //111

        //0 0 0 R V4 V5 V6
        for (i=0;i!=7;i++){
        putbitMMX(length+i,false);
        }
 
        length=length+7;  //118
    
       //0 1 0 F3 F2 F1 F0
        putbitMMX(length,false);
        putbitMMX(length+1,true);

        for (i=2;i!=7;i++){
        putbitMMX(length+i,false);
        }
        length=length+7;  //125

        crc8(init,length);
        length=length+8;   //133  ///THIS VALUE!!!
        init=length;
        //sendMMXCommand(length+8); 
        //length=0;
    
        //A07:0 Such mit 0 Bits von Id-Maske 0x0   53 bits + crc
    
        broadcastAddress(length);
        length=length+9;   //142

        //Comando: 1 1 1 0 1 0
         for (i=0;i!=3;i++){
            putbitMMX(length+i,1);
        }
        putbitMMX(length+3,0);
        putbitMMX(length+4,1);
        putbitMMX(length+5,0);
        length=length+6;    //148

        //n bits=0
        for (i=0;i!=6;i++){
            putbitMMX(length+i,0);         
        }
        length=length+6; //154

        //UID (32 bits))
        buffer[1]=0;
        buffer[2]=0;
        buffer[3]=0;
        buffer[4]=0;

        deco_zentral_uid(length);
        length=length+32;  //186
        crc8(init, length);
        length=length+8;   //194  ///THIS VALUE!!!
        init=length;
    
        //sendMMXCommand(length+8);  //170
        sendMMXCommand(0,31);  
        sendMMXCommand(31,102);
        sendMMXCommand(102,133);
        sendMMXCommand(133,length); //lneght 194
    
        ResponseMMX();
     */

    //test2
    //    ResponseMMX();
    __delay_ms(5);

    //IddleTrack();

    status = CYCLEON;

    /*TEST1
          PinSignalZ;
          //__delay_ms(2000); //2ms no data
        
        
          char i; 
          char j;
          for (i=0;i!=2;i++){
          buffer[0]=0; //no needed for mfx test
          buffer[1]=0xBB;     //centeal
          buffer[2]=0xBB;
          buffer[3]=0xBB;
          buffer[4]=0xBB;
          buffer[5]=0;        //session
          buffer[6]=0;
          buffer[7]=0; //not used for mfx test
          zentral_bake();
          __delay_us(TIMERESPONSE);         //For instance.. a delay not necesarilly linked to the 6400us required by resposne
   
    
          PinSignalOUT;
          for (j=0;j!=10;j++){
         // PinSignal=~PinSignal;
         //  __delay_us(10000); //100mse with a clock.. and no message
          idlePacket(NORMALpreambleBitsNMRA);
          }
          //TRISA = TRISAconf;
          PinSignalZ;
     
         }
       
          PinSignalOUT;
          for (i=0;i!=2;i++){
          buffer[0]=0; //no needed for mfx test
          buffer[1]=0x0;     //Uid
          buffer[2]=0x0;
          buffer[3]=0x0;
          buffer[4]=0x0;
          buffer[5]=0;        //range
          buffer[6]=0; //not used for mfx test
          lok_discovery();
          __delay_us(TIMERESPONSE);
       
          //TRISA = TRISAout;
         PinSignalOUT;
          for (j=0;j!=10;j++){
         // PinSignal=~PinSignal;
         //  __delay_us(10000); //100mse with a clock.. and no message
          idlePacket(NORMALpreambleBitsNMRA);
          }
          //TRISA = TRISAconf;
          PinSignalZ;
       
          }

          PinSignalOUT;
          buffer[0]=0; //no needed for mfx test
          buffer[1]=0x0;     //Uid
          buffer[2]=0x0;
          buffer[3]=0x0;
          buffer[4]=0x0;
          buffer[5]=1;        //range
          buffer[6]=0; //not used for mfx test
          lok_discovery();
          __delay_us(TIMERESPONSE);
       
          //TRISA = TRISAout;
    
          PinSignalOUT;
          for (j=0;j!=10;j++){
         // PinSignal=~PinSignal;
         //  __delay_us(10000); //100mse with a clock.. and no message
          idlePacket(NORMALpreambleBitsNMRA);
          }
          //TRISA = TRISAconf;
          PinSignalZ;
        
        
          PinSignalOUT;
          buffer[0]=0; //no needed for mfx test
          buffer[1]=0x80;     //Uid
          buffer[2]=0x0;
          buffer[3]=0x0;
          buffer[4]=0x0;
          buffer[5]=1;        //range
          buffer[6]=0; //not used for mfx test
          lok_discovery();
          __delay_us(TIMERESPONSE);
        
          //TRISA = TRISAout;
         PinSignalOUT;
          for (j=0;j!=10;j++){
         // PinSignal=~PinSignal;
         //  __delay_us(10000); //100mse with a clock.. and no message
          idlePacket(NORMALpreambleBitsNMRA);
          }
          //TRISA = TRISAconf;
        
          PinSignalZ;
        
          //Security for te decoder... 120ms between consequitive readings
        //  __delay_us(120000);
  /*IF used, normal behaviour is affected, since bufferIN is tretted as a new message... this test does not seem valid any more    
        bufferIN[0]=0;    //no need for this mfx test;
        bufferIN[1]=0;    //address byte 1
        bufferIN[2]=1;   //address byte 2
        bufferIN[3]=20;
        bufferIN[4]=0b10101010;
        bufferIN[5]=0b00000001;
        bufferIN[6]=0;    //no needed for this test
        locoMMX(); 
     */
    /*  bufferIN[0]=0;    //no need for this mfx test;
      bufferIN[1]=0;    //address byte 1
      bufferIN[2]=1;   //address byte 2
      bufferIN[3]=20;
      bufferIN[4]=0b10101010;
      bufferIN[5]=0b00000001;
      bufferIN[6]=0;    //no needed for this test

      locoMMX();*/
    //  di();
    /*  old_status = CYCLEON;
      status=NEWCMD;
      bufferIN[0]=0x64;    //no need for this mfx test;
      bufferIN[1]=34;    //address byte 1
      bufferIN[2]=4;   //address byte 2
      bufferIN[3]=3;
      bufferIN[4]=0xa3;*/
    // locoMM();

    //--------------------------------------------------------

}