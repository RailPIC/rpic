/*
 * File:   nmra.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * QUEUE Routines
 *
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */


#include "user.h"           //access to bit and byte functions
#include "queue.h"          
#include "serial.h"         //buffer is here
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/
queue locosMessageQueue;

void initQueue(void) {
    int i;

    for (i = 0; i < NQUEUES; i++) {
        locosMessageQueue.position[i] = EMPTYQUEUE;
        locosMessageQueue.init[i] = EMPTYQUEUE;
    }

    locosMessageQueue.positionMMFgroup = 1;
    locosMessageQueue.nextFree = 0;

    for (i = 0; i < QUEUESIZE; i++) {
        locosMessageQueue.bytes[i] = 0;
    }
}

int findatQueue(char subQueue) {

    int address = 0;
    //By default positiontowrite is nextfree
    int positiontoWrite = locosMessageQueue.nextFree;
    int nextpositiontoWrite;
    bool found=false;
    
    if (locosMessageQueue.init[subQueue] == EMPTYQUEUE) {
        //if queue empty.. new package and init subque
        locosMessageQueue.init[subQueue] = locosMessageQueue.nextFree;
        locosMessageQueue.position[subQueue] = locosMessageQueue.init[subQueue];
     //   locosMessageQueue.bytes[positiontoWrite]=buffer[0];  //Necessary to init type to calculate next free
    } else {
        //if subqueue not empty
        //get loco address kwnowing command and type
        if (buffer[0] < LOCODCCL14) {
            address = buffer[1];
        } else {
            address = buffer[2];
            byte_of(address, 1) = buffer[1];
        }
        //look in subqueues
        nextpositiontoWrite = locosMessageQueue.init[subQueue];
        int comp_address = getlocoMessageAddress(nextpositiontoWrite);

        do {

            positiontoWrite = nextpositiontoWrite;
            if ((address == comp_address)&&(buffer[0] == locosMessageQueue.bytes[positiontoWrite]))  {
          //      if (buffer[0] != locosMessageQueue.bytes[positiontoWrite]) return EMPTYQUEUE;
                //Both type and address must bt the same. The difference queue allows to have repeated address in each sub-queue...
                //But I also want to have repeated addresses in MM1 and MM2 or most critical in DCCNxx and DCCNFx... Without this, the fucntion packetes are not stored
                //Other mechanisms to store a single package per loco (for DCC) will force to update all fn functions (similar to MM or MFX)... leading to a huge amount of packets on the rails
                //The disadventage of this approach is that a loco may change type tithout going though the specific "change protocol" fucntions ane lead
                //to a duplicated address in the queue qith incompatible protocols (DDCN and DCCL)... this will only last until next quee reset.
                found=true;
                break;
            }
            //Potential error if break is not working leading out of do

            nextpositiontoWrite = getlocoMessageNext(positiontoWrite);
            comp_address = getlocoMessageAddress(nextpositiontoWrite);

        } while (nextpositiontoWrite != locosMessageQueue.init[subQueue]);
    }

    if (!found) {
        //end of queue and not found or init of queue and not searched
       // char last = getlocoMessageLength(positiontoWrite);
        //if not searched (init queue)... nothing more to do.. .if end of queue... it is necessary to
        //update paquete (positiontowrite ) to point next free
        char last;
        if (positiontoWrite!=locosMessageQueue.nextFree){//This happpens when initi queue only.. otherwise postiintowrite is alwayis poiting to an existing packet
        last = getlocoMessageLength(positiontoWrite);
        locosMessageQueue.bytes[positiontoWrite + last - 1] = byte_of(locosMessageQueue.nextFree, 0);
        locosMessageQueue.bytes[positiontoWrite + last - 2] = byte_of(locosMessageQueue.nextFree, 1);
        positiontoWrite = locosMessageQueue.nextFree;
//        locosMessageQueue.bytes[positiontoWrite]=buffer[0];
//        last = getlocoMessageLength(positiontoWrite);
        }
        //If new packet, the new packet will have to point to ini of the queue
        locosMessageQueue.bytes[positiontoWrite]=buffer[0];
        last = getlocoMessageLength(positiontoWrite);

        locosMessageQueue.bytes[positiontoWrite + last-1]=byte_of(locosMessageQueue.init[subQueue], 0);
        locosMessageQueue.bytes[positiontoWrite + last-2]=byte_of(locosMessageQueue.init[subQueue], 1);
        //Suponiendo que despues viene un update.. aqu�i marco donde va el nuevo nextFree
        locosMessageQueue.nextFree = positiontoWrite + last;
    }

    return positiontoWrite;
}

char getlocoMessageLength(int position) {

    char type = locosMessageQueue.bytes[position];

    if ((type > LOCOMM2)&&(type < LOCODCCNfx)) return MESSAGETYPE1SIZE;
    else if ((type < LOCODCCN14) || (type < LOCODCCLfx)) return MESSAGETYPE2SIZE;
    else if (type < LOCOMMX_F4) return MESSAGETYPE3SIZE;
    else if (type < LOCOMMX_F16) return MESSAGETYPE4SIZE;
    else if (type < LOCOMMX_FN) return MESSAGETYPE5SIZE;
    else return (MESSAGETYPE6SIZE + locosMessageQueue.bytes[position + 4]);

}

int getlocoMessageNext(int position) {

    char length = getlocoMessageLength(position);
    int nextPosition;
    nextPosition = locosMessageQueue.bytes[position + length - 1];
    byte_of(nextPosition, 1) = locosMessageQueue.bytes[position + length - 2];

    return nextPosition;
}

int getlocoMessageAddress(int position) {
    char type;
    int address;
    type = locosMessageQueue.bytes[position];

    if (type < LOCODCCL14) {
        address = locosMessageQueue.bytes[position + 1];
    } else {
        address = locosMessageQueue.bytes[position + 2];
        byte_of(address, 1) = locosMessageQueue.bytes[position + 1];
    }
    return address;
}

char updateQueue(int position) {
    //based on type... store in one way or another.. look functions hereafter
    //and also look diagrams vsd.
    char end;

    //Needed for new meesasages to calculate length.. old ones already have buffer 1 that isnot updated
    //locosMessageQueue.bytes[position] = buffer[0];
    //for all messages
    char length = getlocoMessageLength(position);

    if (position + length > QUEUESIZE - 1) return ERROR30; //no more room in queue for new messages

    if (buffer[0] < LOCOMMX_F4) end = length - 2;
    else if (buffer[0] < LOCOMMX_FN) end = length - 6;
    else {
        char nfbytes = locosMessageQueue.bytes[position + 4];
        //end = length-6-1; //Special case_MFX_Fn //must be 4
        end = 4;
        char fchanged = 0b0111111 & buffer[4];
        //char nbyte=globalbit/8;               //common division requires a lot of time... makes impossible to code a bit
        char nbyte = fchanged >> 3;
        //char nbit=globalbit%8;
        char nbit = fchanged & 0b00000111; //mod is the carry http://robertgawron.blogspot.com.es/2011/02/modulo-operation-using-bitwise.html
        if (testbit(buffer[4], 7)) { //function state coded at the MSB of buffer4
            setbit(locosMessageQueue.bytes[position + length - 6 + nfbytes - nbyte - 1], nbit);
        } else {
            clrbit(locosMessageQueue.bytes[position + length - 6 + nfbytes - nbyte - 1], nbit);
        }
    }
    for (char i = 1; i < end; i++) {
        //buffer[0] can not change.. ot could breake the queue.. a spccific procedure is needed for modficiation of loco type
        locosMessageQueue.bytes[position + i] = buffer[i];
    }
  

    return OK;
}

void getfromQueue(int position) {

    char length = getlocoMessageLength(position);
    char end;
    buffer[0] = locosMessageQueue.bytes[position];
    if (buffer[0] < LOCOMMX_F4) end = length - 2;
    else if (buffer[0] < LOCOMMX_FN) end = length - 6;
        //else end = 4; //in LocoMMX the spcial treatment of LOCOMMX_FN will be needed when getting out of queue
    else end = length - 6 + locosMessageQueue.bytes[position + 4];

    for (char i = 1; i < end; i++) {
        //buffer[0] can not change.. ot could breake the queue.. a spccific procedure is needed for modficiation of loco type
        buffer[i] = locosMessageQueue.bytes[position + i];
    }
}

void getfromQueueMMXUID(int position) {
    //This is call aftet a normal getfromQueue
    //necessary to call again length... though buffer[0] is already calculated
    //to make it more universal I could calculate buffer[0]again... thugh adress is always coming from somewehere else
    char length = getlocoMessageLength(position);
    char init = length - 6;
    /*    if (buffer[0] < LOCOMMX_FN) init = length - 6;
        else{
            char nfbytes = locosMessageQueue.bytes[position + 4];
            init=position + length - 6 + nfbytes;
        }*/

    for (char i = 0; i < 4; i++) {
        buffer[3 + i] = locosMessageQueue.bytes[position + init + i];
    }
}

//char getMMoldDspeed() {
int getMMoldData(){
    //Usar find form subqueeu... ver si lo encuntro devolver speeed... si no lo encuentro �que devuelvo?
    //que pasa con primer mensaje... find devuelve... next free (que puede ser primer mensaje... o otra cosa
    //si devuelve algo distinto a next free o empty queue entonces ha encontrado algo    
    //char oldSpeed=0; //if new message... let0s assume init speed=0;
    int result=0;
    int posToRead = findatQueue(QUEUE_MM);

    if (posToRead != locosMessageQueue.nextFree){
        //oldSpeed=locosMessageQueue.bytes[posToRead + 2];//no new message 
         byte_of(result, 0) = locosMessageQueue.bytes[posToRead + 2]; //speed
         byte_of(result, 1) = locosMessageQueue.bytes[posToRead + 3]; //fx
    } 
   // if (posToRead == locosMessageQueue.nextFree)//if not found 
   updateQueue(posToRead); //after a find with a negativ eresults (not found), the space to write net is already reserved.. it is necessary to use it

    //return oldSpeed;
   return result;

}
/*
char getMMoldFx() {
    //So far only for MM2FX

    int posToRead = findatQueue(QUEUE_MM);

    return locosMessageQueue.bytes[posToRead + 3];
    //No need getMMoldspeed is always called before
    /*
   if (posToRead!=locosMessageQueue.nextFree){ //if not found returns empty
       return locosMessageQueue.bytes[posToRead+3];
   }else { 
       //            updateQueue(posToRead); //after a find with a negativ eresults (not found), the space to write net is already reserved.. it is necessary to use it
                               //otherwise two consequtive finds will leva an empty messages reservation  
       return 0;//by default no speed   
   }/*
     
}
*/
void initMMXQueue() {
    //At this stage system si not wathing the init with different address same UID...Shoul not happen.. loco will only respond to the loco wt the right ID

    buffer[0] = buffer[3]; //Update typr to init correctly bytes space;
    int position = findatQueue(QUEUE_MMX); //This will init the space or will return the position to write....as far as buffer[0] is type, 1 and 2 address and 4 Fn (if any) 
    
   // locosMessageQueue.bytes[position]=buffer[0]; //necessary when init queue,,, getlocomessage length looks in this... it is similar to update
    
    for (char i=0;i<3;i++){
        locosMessageQueue.bytes[position + i] = buffer[i];  //iInit type and address
    }
    char length = getlocoMessageLength(position);
    char initMMX=length-6;
    
    for (char i = 3; i < initMMX; i++) {
        locosMessageQueue.bytes[position + i] = 0;  //iInit speed and functions to zero
    }

    for (char i = 0; i < 4; i++) {
        locosMessageQueue.bytes[position + initMMX+ i] = buffer[5 + i];
    }
    
    if (buffer[0]==LOCOMMX_FN) locosMessageQueue.bytes[position+4]=buffer[4]; 
}
