/* 
 * File:   program.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 2 de abril de 2013, 20:10
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Programs Definitions
 *
 */

#ifndef PROGRAM_H
#define	PROGRAM_H

#ifdef	__cplusplus
extern "C" {
#endif

#define numberIDLEpackets 20
#define numberRESETpackets 5
#define numberPROGpackets 20
#define numberPAGEpackets 5

//Definition of command and type
#include "commands.h"
  
//#define PTidle    TMR2ON      //TMR2ON enables or disables timer.. and therfore PWM outmut
//extern bit PTactive;
//extern bit PTrequired;
//extern bit PTwasON;

char program(void);
char getCVNMRA(char bytesused);
char getValueNMRA(char bytesused);
//void PTon();
//void PToff();
//void PTrestore();
//void PTreadyMM();
//void PTreadyNMRA();
//void PTreadyMMX();
//void PTready_ACC_MM();
//void PTready_ACC_NMRA();



#ifdef	__cplusplus
}
#endif
#endif	/* PROGRAM_H */



