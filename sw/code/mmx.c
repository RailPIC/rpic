/*
 * File:   mmx.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MMX Routines
 *
 * The definition of the protocol was obtained from:
 * "Das mfx-Schienenformat"
 * StefanáKrau▀, Version 1 2009-02-01
 * http://www.skrauss.de/modellbahn/mdigital.html
 * Rainer Mueller web site
 * http://www.alice-dsl.net/mue473/mfxrahmen.htm
 *
 *
 * alpha version... it isnot returning a char like locoMM or locoNMRA...no treatment of error
 * There is no update in the queue so far
 * there is no short message if address is short or if speed short... or if no so many functions (different type of mmx paquets too)
 *  possible to save space.. sums lenght+x... this is actually a fix numver.. no need to make an add
 * No get address and so on... if it worked... potentially for new version with different types of MMX
 * alpha versiononly 8 functions.. just to make it compatibel with structure fo queue.. otherwise i need three meesages to store everything
 * if it work, think how to remodel the queue.
 *
 * It ssems the protocol requires the negotiation of an mfx address.
 * MORE RESEARCH NEEDED
 * /

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>

#include "mmx.h"
#include "user.h"           //definition on bit and byte operation
#include "system.h"         //definition on clock for timing
#include "queue.h"          //queue is here
#include "serial.h"         //buffer is here and also abortsignal
#include "syst_config.h"         //buffer is here and also abortsignal

char locoMMX(char start) {

    //The use of lenght as a variable, makes sense when the position of certain
    //data may change...(depedning on number of bits for address for instance)

    //char i;
    //char nextbit=start;
    //First bits 1 1 1 1         A13 A12 A11 A10 A9  A8  A7  A6  A5  A4  A3  A2  A1  A0
    //direccion
    start = mfxAddress(start);
    //Speed
    start = mfxSpeed(start);
    //Functions
    start = mfxFunction(start);

    if (status == NEWCMD) {
           int posToWrite=findatQueue(QUEUE_MMX);
           char result=updateQueue(posToWrite);
           if (result!=OK) return result;       //result now ewll trated
    }

    return start;
}

void zentral_bake(char start) {
    /*    PinSignalOUT;
        //Change for modification Correct init (not sure if still working rest of MFX)
        PinSignal=1;
     */
    //To bs sent periodically (at least 2 commands zentrale needs to bs sent after setting an SID)

    //The use of lenght as a variable, DOES NOT makes sense since the packet lentgh is fixed:
    //63 bits+ CR8
    //char length=0;

    char i;
    char ii;

    //Broadcast Adress 0  bits 1 0         A6  A5  A4  A3  A2  A1  A0
    broadcastAddress(start);
    //length=length+9;

    //Comando: 1 1 1 1 0 1
    for (i = start + 9; i != start + 13; i++) {
        putbitMMX(i, 1);
    }
    putbitMMX(start + 13, 0);
    putbitMMX(start + 14, 1);
    //length=length+6;    //15

    //UUI Zentrale (32 bits))

    //deco_zentral_uid(15);
    for (i = 0; i != 4; i++) {
        for (ii = 0; ii != 8; ii++) {
            //putbitMMX(21+(4-i)*8+(8-ii),testbit(buffer[i],ii-1));         
            putbitMMX(start + 15 + i * 8 + ii, testbit(ZENTRAL[i], 7 - ii));
        }
    }

    //length=length+32; //47

    //Z Session (16 bits))
    for (i = 0; i != 2; i++) {
        for (ii = 0; ii != 8; ii++) {
            //putbitMMX(21+(4-i)*8+(8-ii),testbit(buffer[i],ii-1));         
            putbitMMX(start + 47 + i * 8 + ii, testbit(NSESSION[i], 7 - ii));
        }
    }

    // length=length+16; //63
    /*
        //crc8(length);
        crc8(0,63);

        //Transmit Command
        //sendMMXCommand(71);
        sendMMXCommand(0,71);
     */
    //INIT RESPONSE SPECIFIC
    // ResponseMMX();
    //Normal Ending 4 flags
    /*    for(i=0;i!=4;i++){
        FlagMMX();
        }
     */
    //TRISA = TRISAconf;
    // PinSignalZ;
}

void lok_discovery(char start) {
    /*  PinSignalOUT;
      //Change for modification Correct init (not sure if still working rest of MFX)
      PinSignal=1;
     */
    char i;

    //Broadcast Adress 0  bits 1 0         A6  A5  A4  A3  A2  A1  A0
    broadcastAddress(start);
    //length=length+9;

    //Comando: 1 1 1 0 1 0
    for (i = start + 9; i != start + 12; i++) {
        putbitMMX(i, 1);
    }
    putbitMMX(start + 12, 0);
    putbitMMX(start + 13, 1);
    putbitMMX(start + 14, 0);
    //length=length+6;    //15

    //Range, mask, protocol.. in mmx 0-32.. others are possible from PC to station, not from station to track (no-one will asnwer)
    for (i = start + 15; i != start + 21; i++) {
        putbitMMX(i, testbit(buffer[2], start + 20 - i));
    }
    //length=length+6; //21


    //UID (32 bits))
    deco_uid(start + 21);

    // length=length+32; //53
    //crc8(length);
    //  crc8(0,53);

    //Transmit Command
    //sendMMXCommand(61);
    //  sendMMXCommand(0,61);

    //INIT RESPONSE SPECIFIC
    //  ResponseMMX();

    //TRISA = TRISAconf;
    //PinSignalZ;

}

void bind_nadr(char start) {
    //    PinSignalOUT;
    //Change for modification Correct init (not sure if still working rest of MFX)
    //    PinSignal=1;


    char i;

    //Broadcast Adress 0  bits 1 0         A6  A5  A4  A3  A2  A1  A0
    broadcastAddress(start);
    //length=length+9;

    //Comando: 1 1 1 0 1 1
    for (i = start + 9; i != start + 12; i++) {
        putbitMMX(i, 1);
    }
    putbitMMX(start + 12, 0);
    for (i = start + 13; i != start + 15; i++) {
        putbitMMX(i, 1);
    }
    //length=length+6;    //15

    //SID (14 bits))
    //buffer[5]=buffer[5]&0b00111111; //only 14 bits
    //NO matter address length 14 bits are send (not as ping or any other command with address)
    for (i = 0; i != 6; i++) {
        putbitMMX(start + 15 + i, testbit(buffer[1], 5 - i));
    }
    //length=length+6   //21
    for (i = 0; i != 8; i++) {
        putbitMMX(start + 21 + i, testbit(buffer[2], 7 - i));
    }
    //length=length+8   //29


    //Deco UID (32 bits))
    deco_uid(start + 29);

    //length=length+32; //61

    //crc8(length);
    //    crc8(0,61);

    //Transmit Command
    //sendMMXCommand(69);
    //  sendMMXCommand(0,69);

    //INIT RESPONSE SPECIFIC
    // ResponseMMX();
    //Normal Ending 4 flags
    /*   for(i=0;i!=4;i++){
       FlagMMX();
       }

       //TRISA = TRISAconf;
       PinSignalZ;
     */
}

char ping_verify(char start) {
    char i;
    char length;

    length = mfxAddress(start);

    //Comando: 1 1 1 1 0 0
    for (i = 0; i < 4; i++) {
        putbitMMX(length + i, 1);
    }

    for (i = 0; i < 2; i++) {
        putbitMMX(length + 4 + i, 0);
    }
    length = length + 6;

    //Deco_UID (32 bits))
    deco_uid(length);

    return length + UID_LENGTH;
}

void broadcastAddress(char globalbit) {
    //Broadcast Adress 0  bits 1 0         A6  A5  A4  A3  A2  A1  A0

    putbitMMX(globalbit, 1);
    char i;
    //  for (i=globalbit+1;i!=globalbit+9;i++){
    //    putbitMMX(i,0);
    for (i = 0; i != 8; i++) {
        putbitMMX(globalbit + 1 + i, 0);
    }
}

char mfxAddress(char globalbit) {

    unsigned int adr;
    char lengthaddr = 0;
    //Check if working
    adr = buffer[2];
    byte_of(adr, 1) = buffer[1];
    if ((adr < 1) | (adr > 16383)) {
        return ERROR10;
        //So far no treating error... return value will always be >ERROR10=10
        //ERROR 10 is not a good option... ERROR10=10 can not be a valid result (lenght 10)
    }
    putbitMMX(globalbit, 1);
    globalbit++;
    if (adr < 127) {
        putbitMMX(globalbit, 0);
        globalbit++;
        lengthaddr = 7;
    } else if (adr < 511) {
        putbitMMX(globalbit, 1);
        putbitMMX(globalbit + 1, 0);
        globalbit = globalbit + 2;
        lengthaddr = 9;
    } else if (adr < 2047) {
        putbitMMX(globalbit, 1);
        putbitMMX(globalbit + 1, 1);
        putbitMMX(globalbit + 2, 0);
        globalbit = globalbit + 3;
        lengthaddr = 11;
    } else {
        putbitMMX(globalbit, 1);
        putbitMMX(globalbit + 1, 1);
        putbitMMX(globalbit + 2, 1);
        globalbit = globalbit + 3;
        lengthaddr = 14;
    }
    for (char i = 0; i < lengthaddr; i++) {
        //check if working .. not tested testbit with a two bytes variable
        putbitMMX(globalbit + i, testbit(adr, lengthaddr - 1 - i));
    }
    return globalbit + lengthaddr;
}

char mfxSpeed(char globalbit) {

    //Still pending to know ifll locos accept all types of messages
    //char speed=buffer[3];
    char lengthspeed = 0;
    //0 0
    for (char i = 0; i < 2; i++) {
        putbitMMX(globalbit + i, 0);
    }
    globalbit = globalbit + 2;

    if (buffer[3]&&0b00001111) {
        //short speed low half a byte sould be zero
        //check specification... not clear they have changed the V4 V5 V&.. it should be V6 V5 V4
        putbitMMX(globalbit, 0);
        globalbit++;
        lengthspeed = 4; //3 speed + direction

    } else {
        //long speed
        putbitMMX(globalbit, 1);
        globalbit++;
        lengthspeed = 8; //7 speeed+direction

    }

    for (char i = 0; i < lengthspeed; i++) {
        putbitMMX(globalbit + i, testbit(buffer[3], 7 - i));
    }
    return globalbit + lengthspeed;
}

char mfxFunction(char globalbit) {

    if (buffer[0] < LOCOMMX_FN) {
        //01x
        char nfucntionsperbyte = 8;
        char twobytes = 0;
        
        putbitMMX(globalbit, 0);
        putbitMMX(globalbit + 1, 1);
        globalbit = globalbit + 2;

        switch (buffer[0]) {
            case LOCOMMX_F4:
                //010
                putbitMMX(globalbit, 0);
                globalbit++;
                nfucntionsperbyte = 4;
                break;
            case LOCOMMX_F8:
                //0110
                putbitMMX(globalbit, 1);
                putbitMMX(globalbit+1, 0);
                globalbit = globalbit + 2;
                break;
            case LOCOMMX_F16:
                //0111
                putbitMMX(globalbit, 1);
                putbitMMX(globalbit+1, 1);
                globalbit = globalbit + 2;
                twobytes = 1;
                break;
        }
        
        if (twobytes){
            for (char i=0;i<8;i++){
            putbitMMX(globalbit + i, testbit(buffer[4], 7 - i));
            }
            globalbit = globalbit + 8;
        }
            for (char i=0;i<nfucntionsperbyte;i++){
            putbitMMX(globalbit + i, testbit(buffer[4+twobytes], nfucntionsperbyte-1 - i));
            }
            globalbit = globalbit + nfucntionsperbyte;
    }else{
    //1 0 0 NNNNNNN 0 F
        char nfbytes=buffer[4];
        for (char i=0;i<nfbytes;i++){
            for (char ii=0;ii<8;ii++){
                putbitMMX(globalbit, 1);
                putbitMMX(globalbit+1, 0);
                putbitMMX(globalbit+2, 0);
                globalbit = globalbit + 3;
                char nf=i<<3+ii;    //ii*8+i=n function
                 //F0, F1, F2, F3, F4   //Carefull transmit F0 to F127... but stores F127 to F0
                for (char iii=0;iii<7;iii++){
                   putbitMMX(globalbit + iii, testbit(nf, 6 - iii)); 
                }
            //0 f
            putbitMMX(globalbit+7, 0);
            //f in globalbil+8
            putbitMMX(globalbit + 8, testbit(buffer[4+nfbytes-i], ii)); 
            globalbit=globalbit+9;
            }
        }
    }
    return globalbit;
}

void deco_uid(char globalbit) {

    char i;
    char ii;

    for (i = 0; i != 4; i++) {
        for (ii = 0; ii != 8; ii++) {
            //putbitMMX(21+(4-i)*8+(8-ii),testbit(buffer[i],ii-1));         
            //putbitMMX(globalbit+i*8+ii,testbit(buffer[i+1],7-ii));         

            putbitMMX(globalbit + i * 8 + ii, testbit(buffer[i + 3], 7 - ii));
        }
    }
}

//void crc8(char length){

void crc8(char start, char end) {
    char i;
    int crc = 127;

    for (i = 0; i != 8; i++) {
        putbitMMX(end + i, 0);
    }
    end = end + 8;

    for (i = start; i < end; i++) {
        crc = (crc << 1) + getbitMMX(i);
        if ((crc & 0x100) > 0) {
            crc = crc^0x107;
        }
    }

    for (i = 0; i != 8; i++) {

        putbitMMX(end - 8 + i, testbit(byte_of(crc, 0), 7 - i));
        //putbitMMX(length-8+i,0); //Worng CRC for testing
    }
}

void putbitMMX(char globalbit, char b) {
    //char nbyte=globalbit/8;               //common division requires a lot of time... makes impossible to code a bit
    char nbyte = globalbit >> 3;
    //char nbit=globalbit%8;
    char nbit = globalbit & 0b00000111; //mod is the carry http://robertgawron.blogspot.com.es/2011/02/modulo-operation-using-bitwise.html
    if (b) {
        setbit(byteMMX[nbyte], nbit);
    } else {

        clrbit(byteMMX[nbyte], nbit);
    }
}

bit getbitMMX(char globalbit) {
    //char nbyte=globalbit/8;
    char nbyte = globalbit >> 3; //instead of the  division
    //char nbit=globalbit%8;
    char nbit = globalbit & 0b00000111; //mod is the carry http://robertgawron.blogspot.com.es/2011/02/modulo-operation-using-bitwise.html

    if (testbit(byteMMX[nbyte], nbit)) {
        return 1;
    } else {

        return 0;
    }
}

void FlagMMX(void) {
    if (!abortsignal) {

        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME0MMX);
        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME1MMX);
        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME0MMX);
    }
}

void Bit1MMX(void) {
    if (!abortsignal) {

        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME1MMXH);
        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME1MMXL);
    }
}

void Bit0MMX(void) {
    if (!abortsignal) {

        //PinSignal = ~PinSignal;
        SignalTrackSW();
        __delay_us(TIME0MMXH);
    }
}

void sendMMXCommand(char start, char end) {
    char i;
    char nbit1 = 0;
    //TRISA = TRISAout;
    //  PinSignalOUT;
    //Change for modification Correct init (not sure if still working rest of MFX)
    //PinSignal=1;

    for (i = 0; i != 2; i++) {
        FlagMMX();
    }

    for (i = start; i != end; i++) {
        if (getbitMMX(i)) {
            //check if 8 its... send one cero first
            nbit1++;
            Bit1MMX();
        } else {
            nbit1 = 0;
            Bit0MMX();
        }
        if (nbit1 == 8) {

            Bit0MMX();
            nbit1 == 0;
        }
    }
}

void EndFlag() {
    for (char i = 0; i != 4; i++) {
        FlagMMX();
    }
}

void SimpleResponseMMX() {
    char i;
    //22 flags; 11 sync
    for (i = 0; i != 22; i++) {
        FlagMMX();
    }
    //11,5 sync if needed
    // if (PinSignal==0) FlagMMX();    //PinSignal=0->SIGNAL HIGH. FLAG WILL END IN DOWN (as it should always end)
   // if (PinSignal == 1) FlagMMX(); //PinSignal=0->SIGNAL HIGH. FLAG WILL END IN DOWN (as it should always end)
     if (PinSignalR == 1) FlagMMX(); //PinSignal=0->SIGNAL HIGH. FLAG WILL END IN DOWN (as it should always end)

    //0 0 1 1
    for (i = 0; i != 2; i++) {
        Bit0MMX();
    }
    for (i = 0; i != 2; i++) {
        Bit1MMX();
    }
    
    //PinSignal = ~PinSignal; //conclude before response window
     SignalTrackSW();
    // PinBO=1;
    //    TRISA = TRISAconf;

  
    
    //New routine to get response... it must long TIMERESPONSE
    //Testing we send info on MMX... Question when to read and how to send response.
//    PinSimpleRead=1;
  //  PinMMXout=1;
     //__delay_us(TIMERESPONSE);
    //It coulrbe done with a timer.. but so far I have implemented this option
    char response=NEGATIVE_LIMIT;
    for (char it=0;it<100;it++){
    //100 iterations of 64us= 6400us
        __delay_us(TIMERESPONSE_100);
        if (PinSCOUTin==1)
            response++;
        else
            response--;
    }
  //  PinSimpleRead=0;
  //  PinMMXout=0;
    buffer[MAXBYTES - 1]=1; //one byte response
   
    if (response>POSITIVE_LIMIT)
       buffer[0]=1;
    else
       buffer[0]=0;
  
    //END new routine
    
    
    for (i = 0; i != 2; i++) {
        FlagMMX();
    }

    //if (PinSignal==1) FlagMMX(); //PinSignal=1->SIGNAL LOW. FLAG WILL END IN HOGH (as it should always end in second window)
    //if (PinSignal == 0) FlagMMX(); //PinSignal=1->SIGNAL LOW. FLAG WILL END IN HOGH (as it should always end in second window)
    if (PinSignalR == 0) FlagMMX(); //PinSignal=1->SIGNAL LOW. FLAG WILL END IN HOGH (as it should always end in second window)
    //PinBO=1;
    //PinSignal = ~PinSignal; //conclude before response window
    SignalTrackSW();
    
    __delay_us(TIMERESPONSE);

 
    EndFlag();
}

