/* 
 * File:   timer.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 10 de marzo de 2013, 9:05
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Timers definition
 *
 */

#ifndef TIMER_H
#define	TIMER_H

#define CONFIGTIMER         0       //everything is directly defined in the function. I am enambling the variable  for compatibility with the pic18 code
#ifdef __16F1825
#define TIMERcount      224         //for 32Mhz..Tinst=1,25e-7s. I want 1ms timeout 1ms/1,25e-7/256(prescaler)=31,25=32...256-32=224
#endif
#ifdef __16F18326
//If I undesrtand it right TMR0 in this PIC counts until TMRL matches TMRH.. no overflow
#define TIMERcount      32         //for 32Mhz..Tinst=1,25e-7s. I want 1ms timeout 1ms/1,25e-7/256(prescaler)=31,25=32...256-32=224
#endif
#define OPTION_REGconf  0b10000111  //prescaler 256.. the rest.. not very relevant
#define T0CON0conf     0b00000000  //8 bits not postscaler �Module disabled T0EN
#define T0CON1conf     0b01001000  //Prescaler 256



void OpenTimer0( unsigned char config);
void CloseTimer0(void);
void WriteTimer0(char data);


void startTimeout(void);
void stopTimeout(void);

#endif	/* TIMER_H */




