/*
 * File:   serial.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 3 de marzo de 2013, 18:48
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Serial port routines
 * 
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "serial.h"
#include "timer.h"
#include "program.h"
#include "user.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

char bufferIN[MAXBYTES];


void OpenUSART(unsigned char config, unsigned int spbrg) {

#ifdef __16F1825
    SPBRGL = spbrg;
    SPBRGH=0;
    TXSTA = TXSTAconf;
    RCSTA = RCSTAconf;
#endif
#ifdef __16F18326
    SP1BRGL = spbrg;
    SP1BRGH=0;
    TX1STA = TXSTAconf;
    RC1STA = RCSTAconf;
#endif



    //I do not configure the INT here.. but in the initiINT.. different from PIC18
}

void CloseUSART() {
    SPEN = 0;           //DISABLE TRANSMISSION;
}

void baudUSART(unsigned char baudconfig) {
    //this is not implemented in PIC16 but it is impelmented inPIC16f1825
#ifdef __16F1825
    BAUDCON=baudconfig;
#endif
#ifdef __16F18326
    BAUD1CON=baudconfig;
#endif

}

char ReadUSART(void) {
#ifdef __16F1825
    return RCREG;
#endif
#ifdef __16F18326
    return RC1REG;
#endif
    
}

void WriteUSART(char data) {
    while (TRMT==0);   //Wait TX buffer is ready to send. Potential blcoking function
#ifdef __16F1825
    TXREG = data;
#endif
#ifdef __16F18326
    TX1REG = data;
#endif

    
}


char checksum(char toverify[], char firstbyte, char lastbyte) {
    char i;
    char sum = toverify[firstbyte];

    for (i = firstbyte; i != lastbyte; i++) {
        sum = sum^toverify[i + 1];
    }
    return sum;
}

void bufferFirstByte() {
    bufferIN[0] = ReadUSART();
    //char type = bufferIN[0]&0b00011111;
    //char command = (bufferIN[0] >> 5)&0b00000111;
    
    //char command=bufferIN[0]&0b00001111;
    commandlength=(bufferIN[0]>>4)&0b00001111;
    //bufferIN[0]=command;
    if (commandlength>0){bytesreceived=1;}
}

void bufferNextBytes() {
    bufferIN[bytesreceived] = ReadUSART();
    if (bytesreceived == commandlength) {
        if (status!=NEWCMD){
            old_status = status;    //avoid infinitive loop whnen two consequetive newcmd arrives
        }
        status = NEWCMD;
        if (bufferIN[commandlength] != checksum(bufferIN, 0, commandlength - 1)) {
            bufferIN[1] = ERROR1;   //Checksum error}....commandsystem=0.. type==error1=1.. type est� en bufferin[1]
            //bufferIN[0] = ERROR1;   //Checksum error}....commandsystem=0.. type==error1=1
            
        }
        bufferIN[MAXBYTES - 1] = bufferIN[commandlength]; //In the last byte I always store the checkusm of the message for ID of message
        
        
        if (bufferIN[1]==CYCLEON){
           // InitTrack();
            SignalTrack1();  //needed for MMX start
           // TRISA =TRISAout;    
        }
        if (bufferIN[1]==CYCLEOFF){
            PORTA=0;
          //  TRISA =TRISAconf;    
        }

        
        
        bytesreceived = 0;
        stopTimeout();
    } else {
        bytesreceived++;
    }
}
