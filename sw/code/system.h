/*
 * File:   SYSTEM.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 26 de febrero de 2013, 9:51
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  System Definitions
 *
 * Custom oscillator configuration funtions, reset source evaluation
 * functions, and other non-peripheral microcontroller initialization functions
 * go here.
 */

/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* Microcontroller MIPs (FCY) */

#define SYS_FREQ        32000000            //2000000 for pic16f628a
#if defined(__16F1825)
#define OSCCONconf      0b11110000         //PLL ON.. in confgigbit too....IRCF=1111 (16mhz) 0 SCS=00 clok in configbits
#elif defined(__16F18326)
#define OSCCONconf1     0                   //IntOsc, PLL on
#define OSCFRQconf      0b00000110          //32Mhz
//Still 5 other conf registers... I think no needed to change

#endif


#define _XTAL_FREQ      SYS_FREQ        //used by the delay functions
#define FCY             SYS_FREQ/4

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */
