/*
 * File:   program.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Program Routines
 *
 * The definition of the protocol was obtained from:
 * NMRA website
 * http://www.nmra.org/
 * Standard 9.1
 * Standard 9.2
 * Standard 9.2.1
 * Standard 9.3.2
 * Recommended Practice 9.2.3
 * Recommended Practice 9.3.1
 * 
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

//bit PTactive=0;
//bit PTrequired=0;

//bit PTwasON=0;

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)
#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#endif

#include "program.h";
#include "user.h";      //bit functions are here
#include "nmra.h";      //needed for pom functions getaddress
#include "mm.h";      //needed for delay adter PTOFF
#include "serial.h";    //buffer and checksum
#include "timer.h"         //CCP3CONconf
#include "system.h"         //CCP3CONconf

char program(void) {

    char i;
    char j;
    char bytesused = 0;
    
 
    if (buffer[0] < MM) {
        //currently only NMRA functions

        if (buffer[0] > NMRA_SERVICE_MODE_DIRECT_bit) {
            bytesused = getNMRAaddress();
        }
        bytesused = getCVNMRA(bytesused);
        bytesused = getValueNMRA(bytesused);
        bytesNMRA[bytesused] = checksum(bytesNMRA, 0, bytesused - 1);

        if (buffer[0] > NMRA_SERVICE_MODE_DIRECT_bit) {
        //    InitTrack();
            sendNMRApreamble(NORMALpreambleBitsNMRA);
            //send all bytes
            for (i = 0; i != bytesused + 1; i++) {
                Bit0NMRA();
                sendNMRAbyte(bytesNMRA[i]);
            }
            Bit1NMRA();
        //    IddleTrack();
        } else {
            //service mode
        //    InitTrack();
            for (i = 0; i != numberIDLEpackets; i++) {
                idlePacket(LONGpreambleBitsNMRA);
            }
            for (i = 0; i != numberRESETpackets; i++) {
                resetPacket(LONGpreambleBitsNMRA);
            }
            if (buffer[0] < NMRA_SERVICE_MODE_DIRECT_write) {
                if (buffer[0] < NMRA_SERVICE_MODE_PAGE_write) {
                    for (i = 0; i != numberPAGEpackets; i++) {
                        pagePacket(LONGpreambleBitsNMRA);
                    }
                } else {
                    for (i = 0; i != numberPROGpackets; i++) {
                        sendNMRApreamble(LONGpreambleBitsNMRA);
                        for (j = 0; j != bytesused + 1; j++) {
                            Bit0NMRA();
                            sendNMRAbyte(bytesNMRA[j]);
                        }
                        Bit1NMRA();
                    }
                    buffer[2] = buffer[1];
                    bytesused = 0;
                    bytesused = getCVNMRA(bytesused);
                    bytesused = getValueNMRA(bytesused);
                    bytesNMRA[bytesused] = checksum(bytesNMRA, 0, bytesused - 1);
                }

                for (i = 0; i != numberRESETpackets * 2; i++) {
                    resetPacket(LONGpreambleBitsNMRA);
                }
            }
            //Currently no difference between writign and reading... 20 packetes... we should listen then to information
            for (i = 0; i != numberPROGpackets; i++) {
                sendNMRApreamble(LONGpreambleBitsNMRA);
                //send all bytes
                for (j = 0; j != bytesused + 1; j++) {
                    Bit0NMRA();
                    sendNMRAbyte(bytesNMRA[j]);
                }
                Bit1NMRA();
            }
        //    IddleTrack();
            //PTrequired=0;
        }
        //if any reading procedure...it should be here
    }else{
    //MM and MMX programming
        return ERROR20;
    }
    
   // if (PTactive){  //Restart PWM
//    PTrestore();
    
    return OK;
}

char getCVNMRA(char bytesused) {

    char cc;
    char prog1;
    char cmd;
    char bytesusedaft = 1;
    //So far, only NMRA

    if (buffer[0] < ACCNMRAwrite) {
        cmd = 0b01110000; //Service mode cmd code
    } else {
        cmd = 0b11100000; //Operation mode cmd code (pom)
    }

    if (buffer[0] > NMRA_SERVICE_MODE_PAGE_read) {
        bytesNMRA[bytesused + 1] = buffer[commandlength - 2];
        bytesNMRA[bytesused] = 0b00000011 & buffer[commandlength - 3];
        bytesusedaft = 2;
    } else if (buffer[0] > NMRA_SERVICE_MODE_ADDRESS_read) {
        bytesNMRA[bytesused] = 0b00000111 & buffer[commandlength - 2];
    } else {
        bytesNMRA[bytesused] = 0; //address mode
    }
    switch (buffer[0]) {
        case NMRA_SERVICE_MODE_DIRECT_write:
            cc = 0b11;
            break;
        case NMRA_SERVICE_MODE_DIRECT_verify:
            cc = 0b01;
            break;
        case NMRA_SERVICE_MODE_DIRECT_bit:
        case NMRA_SERVICE_MODE_ADDRESS_write:
        case NMRA_SERVICE_MODE_REGISTER_write:
        case NMRA_SERVICE_MODE_PAGE_write:
            cc = 0b10;
            break;
        case NMRA_SERVICE_MODE_ADDRESS_read:
        case NMRA_SERVICE_MODE_REGISTER_read:
        case NMRA_SERVICE_MODE_PAGE_read:
            cc = 0b00;
            break;
    }

    prog1 = (cc << 2)&0b00001100;
    prog1 = cmd | prog1;
    bytesNMRA[bytesused] = prog1 | bytesNMRA[0];
    return bytesused + bytesusedaft;
}

char getValueNMRA(char bytesused) {
    //vas por aqui.. repasa que el formato es correcto... creo que s�... solo tiene que llegar en el formato correcto
    bytesNMRA[bytesused] = buffer[commandlength - 1];
    return bytesused + 1;
}
