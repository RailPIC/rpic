/*
 * File:   interrupts.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 6 de abril de 2013, 11:42
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
  * Interrupts routines
 *
 */

/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#endif

#include "serial.h"         //buffer is here and definition of commands
#include "timer.h"          //timeout management

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Note that some PIC16's
 * are baseline devices.  Unfortunately the baseline detection macro is
 * _PIC12 */
#ifndef _PIC12

char commandlength;
char bytesreceived;


void interrupt isr(void)
{
    /* This code stub shows general interrupt handling.  Note that these
    conditional statements are not handled within 3 seperate if blocks.
    Do not use a seperate if block for each interrupt flag to avoid run
    time errors. */

    if (RCIF == 1) {
        if (OERR){                  //To solve when meessages are sent too fast
           CREN=0;
           CREN=1;
       }
        if (bytesreceived == 0) {
            bufferFirstByte();
            startTimeout();
            abortsignal=true;       //during recpetion of message no signal is sent to the raul aprox (0,6ms)
        } else {                    //it has to be this way.. otherwise..the storgae of the bytes may extend the delay of a bit being executed... leaidng to a misscodign of the bit
            bufferNextBytes();      //If it is the last byte.. buffernext will change buffer bytesrecieved to 0
          }
      } else if (TMR0IF==1){
        bytesreceived = 0;
        stopTimeout();
        TMR0IF=0;
    }
}
#endif


