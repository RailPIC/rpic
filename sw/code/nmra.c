/*
 * File:   nmra.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * NMRA Routines
 *
 * The definition of the protocol was obtained from:
 * NMRA website
 * http://www.nmra.org/
 * Standard 9.1
 * Standard 9.2
 * Standard 9.2.1
 * Standard 9.3.2
 * Recommended Practice 9.2.3
 * Recommended Practice 9.3.1
 * 
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "nmra.h"
#include "user.h"           //definition on bit and byte operation
#include "system.h"         //definition on clocl
#include "queue.h"          //queue is here
#include "serial.h"         //buffer is here and also abortsignal
#include "program.h"         //PTactive
#include "timer.h"         //CCP3CONconf
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

char bytesNMRA[MAXBYTESNMRA]; //5 is enough  6 bytes only needed for writing cv in pom... not in this version

char locoNMRA() {
    char bytesused = 0;
    char i;

    //not optimised codewise, but easy to read (I hope)
    bytesused = getNMRAaddress();
    if (bytesused == ERROR10) {
        return ERROR10;
    }

    //no errot init track
    SignalTrack0();     //It seems it is needed...??
    //__delay_us(TIME0NMRAH);
    //PORTA=0;
   // InitTrack();

    if ((buffer[0] == LOCODCCNfx) | (buffer[0] == LOCODCCLfx)) {
        //message fx
        bytesused = getNMRAfx(bytesused);
    } else {
        //if it is not a message for fucntions, it is a message for speed
        bytesused = getNMRAspeed(bytesused);
    }
    bytesNMRA[bytesused] = checksum(bytesNMRA, 0, bytesused - 1);

    if (status == NEWCMD) {
        int posToWrite = findatQueue(QUEUE_NMRA);
        char result = updateQueue(posToWrite);
        if (result != OK) return result;
    }


    //Init transmission
    sendNMRApreamble(NORMALpreambleBitsNMRA);
    //send all bytes
    for (i = 0; i != bytesused + 1; i++) {
        Bit0NMRA();
        sendNMRAbyte(bytesNMRA[i]);
    }
    Bit1NMRA();

    //IddleTrack();
     SignalTrack1();

    return OK;
}

char accNMRA() {
    char bytesused = 0;
    char i;

    bytesused = getNMRAaddress();
    if (bytesused == ERROR10) {
        return ERROR10;
    }

    //no errot init track
    SignalTrack0();     //It seems it is needed...??
    //InitTrack();

    bytesused = getNMRAacc(bytesused);
    bytesNMRA[bytesused] = checksum(bytesNMRA, 0, bytesused - 1);

    sendNMRApreamble(NORMALpreambleBitsNMRA);
    //send all bytes
    for (i = 0; i != bytesused + 1; i++) {
        Bit0NMRA();
        sendNMRAbyte(bytesNMRA[i]);
    }
    Bit1NMRA();

    //IddleTrack();
    SignalTrack1();

    return OK;
}

char getNMRAaddress() {
    if (((buffer[0] >= LOCODCCN14)&(buffer[0] <= LOCODCCNfx)) | ((buffer[0] >= LOCODCCNwrite)&(buffer[0] <= LOCODCCNbit))) {
        //short address loco 0AAAAAAA

        //if ((buffer[1] < 1) | (buffer[1] > 127)) {
        if ( (buffer[1] > 127)) {       //In order to alow broadcast address
            return ERROR10;
        }
        bytesNMRA[0] = buffer[1];
        return 1; //1 byte used
    }
    if (((buffer[0] >= LOCODCCL14)&(buffer[0] <= LOCODCCLfx)) | ((buffer[0] >= LOCODCCLwrite)&(buffer[0] <= LOCODCCLbit))) {
        //long address loco based 11AAAAAA AAAAAAAA
        //recieved (buffer(1) (buffer(2)=int
        int check;
        check = buffer[2];

        byte_of(check, 1) = buffer[1];
        if ((check < 1) | (check > 16384)) {
            return ERROR10;
        }

        bytesNMRA[0] = (buffer[1]) | 0b11000000; //first byte has the 6MSB //the OR is the format
        bytesNMRA[1] = buffer[2];

        return 2;
    }
    if ((buffer[0] == ACCNMRA) | ((buffer[0] >= ACCNMRAwrite)&(buffer[0] <= ACCNMRAbit))) {
        //accesory address.                                   //no extended decoders so fat
        //recieved (buffer(1) (buffer(2)=int
        int check;
        check = buffer[2];
        byte_of(check, 1) = buffer[1];
        if ((check < 0) | (check > 511)) {
            return ERROR10;
        }
        char dummy;
        bytesNMRA[0] = buffer[2];
        setbit(bytesNMRA[0], 7);
        clrbit(bytesNMRA[0], 6);
        //first byte has the 6LSB The set and clear is the format 10AAAAAA
        dummy = (buffer[2] >> 2)&0b00110000; //recover AAXXXXX and move to its right position
        if (testbit(buffer[1], 0)) { //bit 9 is the LSB of buufer[1]
            setbit(dummy, 6);
        } else {
            clrbit(dummy, 6);
        }
        bytesNMRA[1] = ~dummy & 0b11110000; //one's complementof 0AAA=1AAA. First nible done

        return 2;
    }
    return ERROR10; //actually this means an error.. or empty queue (normal situation) or corrupted queue or the processing of bufferIN is wring
}

char getNMRAspeed(char startbyte) {
    char speed;
    speed = buffer[1 + startbyte]&0b01111111; //remove the bit of direction
    switch (buffer[0]) {
        case LOCODCCN14:
        case LOCODCCL14:
            //depending on  Long address or not, the speed is included in a startbyte or another
            bytesNMRA[startbyte] = 0b00011111 & speed; //we keep5 5 bits CSSSS...C shoudl be the lights for old decoders...up to rocrail to notify this right
            //DDX assumes is always 1...in my case.. whatever we have in speed is what will be transmitted
            setbit(bytesNMRA[startbyte], 6); //Speed code 01DCSSSS
            if (testbit(buffer[1 + startbyte], 7)) { //direction bit
                setbit(bytesNMRA[startbyte], 5);
            } else {
                clrbit(bytesNMRA[startbyte], 5);
            }
            return startbyte + 1;
        case LOCODCCN28:
        case LOCODCCL28:
            if (speed != 0) {
                speed = speed + 3;
            } //first 3 adress after 00000 are reserved to stop or smergency stop
            speed = (speed >> 1) | (speed << 7); //LSB is in C bit...we rotate to keep this bit in the MSB of speed
            bytesNMRA[startbyte] = 0b00001111 & speed; //we keep4
            setbit(bytesNMRA[startbyte], 6); //Speed code 01DCSSSS
            if (testbit(speed, 7)) { //put LSB
                setbit(bytesNMRA[startbyte], 4);
            } else {
                clrbit(bytesNMRA[startbyte], 4);
            }
            if (testbit(buffer[1 + startbyte], 7)) { //directio bit
                setbit(bytesNMRA[startbyte], 5);
            } else {
                clrbit(bytesNMRA[startbyte], 5);
            }
            return startbyte + 1;
        case LOCODCCN128:
        case LOCODCCL128:
            bytesNMRA[startbyte] = 0b00111111; //Code for extended speed.
            bytesNMRA[startbyte + 1] = speed;
            if (testbit(buffer[1 + startbyte], 7)) { //directio bit
                setbit(bytesNMRA[startbyte + 1], 7);
            } else {
                clrbit(bytesNMRA[startbyte + 1], 7);
            }
            return startbyte + 2;
        default:
            return 0;
    }
}

char getNMRAfx(char startbyte) {
    switch (buffer[2 + startbyte]) {
        case 0: //F0F4F3F2F1
            bytesNMRA[startbyte] = 0b00001111 & (buffer[1 + startbyte] >> 1); //received F4D3F2F1F0..so we move 1 to the right
            if (testbit(buffer[1 + startbyte], 0)) { //we recover F0 from the original tranmsission
                setbit(bytesNMRA[startbyte], 4);
            } else {
                clrbit(bytesNMRA[startbyte], 4);
            }
            bytesNMRA[startbyte] = 0b10000000 | bytesNMRA[startbyte]; //code group 0 is 100F0F4F3F2F1
            return startbyte + 1;
        case 1:
            bytesNMRA[startbyte] = 0b00001111 & buffer[1 + startbyte]; //keep the last 4 bits
            bytesNMRA[startbyte] = 0b10110000 | bytesNMRA[startbyte]; //code group 1 is 1011F8F7F6F5
            return startbyte + 1;
        case 2:
            bytesNMRA[startbyte] = 0b00001111 & buffer[1 + startbyte]; //keep the last 4 bits
            bytesNMRA[startbyte] = 0b10100000 | bytesNMRA[startbyte]; //code group 2 is 1010F12F11F19F9
            return startbyte + 1;
        case 3:
        case 4:
            bytesNMRA[startbyte] = 0b11011110; //code group 3 and 4 is 11011110 and then the F20 to F13
            bytesNMRA[startbyte + 1] = buffer[1 + startbyte];
            return startbyte + 2;
        case 5:
        case 6:
            bytesNMRA[startbyte] = 0b11011111; //code group 3 and 4 is 11011111 and then the F28 to F21
            bytesNMRA[startbyte + 1] = buffer[1 + startbyte];
            return startbyte + 2;
        default:
            return 0; //actually this means an error
    }
}

char getNMRAacc(char startbyte) {
    //I still give the startbyte preparing for future updates.. so far the startbyet is always the same in this kind of packets
    //In this case I need the buffer to have the exact same code as the NMRA CDDD C=1/0 the status DDD the port...
    //Sending the expeected F7F6F5F4F3F2F1F0 bits..as for the loco... give not enough information of which port the packet is for.
    //And it is not a good strategy to translate a message into 8 packets (1 per port).
    //The alternative is to send an additional packet as for the lokos (the fgroup)... making everything more complexe
    char dummy;
    dummy = 0b00001111 & buffer[1 + startbyte];
    bytesNMRA[startbyte - 1] = bytesNMRA[startbyte - 1] | dummy;
    return startbyte;
}

void idlePacket(char preambleBitsNMRA) {
    // 11111111111111__	0	11111111	0	00000000	0	11111111	1
    char i;
    sendNMRApreamble(preambleBitsNMRA);
    // 11111111111111	0__	11111111	0	00000000	0	11111111	1
    Bit0NMRA();
    // 111111111111	0	11111111__	0	00000000	0	11111111	1
    for (i = 0; i != 8; i++) {
        Bit1NMRA();
    }
    // 11111111111111	0	11111111	0__	00000000__	0__	11111111	1
    for (i = 0; i != 10; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0	11111111	0	00000000	0	11111111__	1__
    for (i = 0; i != 9; i++) {
        Bit1NMRA();
    }
}

void broadcastPacket(char speed){
    buffer[0]=LOCODCCN14;   
    //Broadcast address will be 0. The rest of the message, I am assuming a N14 structure
    //it should be enbough for EBreak and Boradcast for railcom
    //Speed and directio messages are not considered by any decoder, unless ebreak
    //And i hope it helps to trigger railcom response.
    buffer[1]=0;//0 address
    buffer[2]=speed; //urrently onluy 0 or 1 should be trated.
    locoNMRA();
}

void resetPacket(char preambleBitsNMRA) {
    // 11111111111111__	0	00000000	0	00000000	0	00000000	1
    char i;
    sendNMRApreamble(preambleBitsNMRA);
    // 11111111111111	0__	00000000	0	00000000	0	00000000	1
    for (i = 0; i != 27; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0	00000000	0	00000000	0	00000000	1__
    Bit1NMRA();
}

void pagePacket(char preambleBitsNMRA) {
    // 11111111111111__	0       01111101        0       00000001        0       01111100        1
    char i;
    sendNMRApreamble(preambleBitsNMRA);
    // 11111111111111	0__       01111101        0       00000001        0       01111100        1
    for (i = 0; i != 2; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0       0__1111101        0       00000001        0       01111100        1
    for (i = 0; i != 5; i++) {
        Bit1NMRA();
    }
    // 11111111111111	0       011111__01        0       00000001        0       01111100        1
    Bit0NMRA();
    Bit1NMRA();
    // 11111111111111	0       01111101        0__       00000001        0       01111100        1
    for (i = 0; i != 8; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0       01111101        0       00000001__        0       01111100        1
    Bit1NMRA();
    // 11111111111111	0       01111101        0       00000001        0__       01111100        1
    for (i = 0; i != 2; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0       01111101        0       00000001        0       0__1111100        1
    for (i = 0; i != 5; i++) {
        Bit1NMRA();
    }
    // 11111111111111	0       01111101        0       00000001        0       011111__00        1
    for (i = 0; i != 2; i++) {
        Bit0NMRA();
    }
    // 11111111111111	0       01111101        0       00000001        0       01111100        1__
    Bit1NMRA();
}

void sendNMRApreamble(char preambleBitsNMRA) {
    char i;
    for (i = 0; i != preambleBitsNMRA; i++) {
        Bit1NMRA();
    }
}

void sendNMRAbyte(char byte) {
    char i;
    for (i = 0; i != 8; i++) {
        if (testbit(byte, 7 - i)) {
            Bit1NMRA();
        } else {
            Bit0NMRA();
        }
    }
}

void Bit0NMRA(void) {
    if (!abortsignal) { //If we are on an interruption we do want to send the message
  //InitTrack and Iddle will be the onesmaking the bits available or not

            //PinSignal = 1;
        SignalTrack1();
        __delay_us(TIME0NMRAH);
             //PinSignal = 0;
            SignalTrack0();
        __delay_us(TIME0NMRAL);
    }
}

void Bit1NMRA(void) {
    if (!abortsignal) {
            //PinSignal = 1;
            SignalTrack1();
        __delay_us(TIME1NMRAL);
             //PinSignal = 0;
            SignalTrack0();
        __delay_us(TIME1NMRAH);
    }
}