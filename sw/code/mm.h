/* 
 * File:   mm.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MM Definitions
 *
 * Timing adapted for the current routines and 32Mhz clock
 * Further timing improvements may lead to new timing definitions
 * Faster cloks also requires new timing definitions
 *
 * Standar timing should be:
 * LOCLONG: 182us
 * LOCSHORT: 26us
 * ACCLONG:  91us
 * ACCSHORT: 13us
 * 
 */

#ifndef MM_H
#define	MM_H

#ifdef	__cplusplus
extern "C" {
#endif

//LOC Timing
#define TIMESHORTMMLOCL          26  //Time short Low
#define TIMELONGMMLOCL          182  //Time long  Low
#define TIMESHORTMMLOCH          17  //Time short High
#define TIMELONGMMLOCH          175  //Time long  High

//ACC Timing
#define TIMESHORTMMACCL          13  //Time short Low
#define TIMELONGMMACCL           91  //Time long  Low
#define TIMESHORTMMACCH          4   //Time short  High
#define TIMELONGMMACCH           84  //Time long   High

//Timing between packes
#define TIMEMMBTW           1525
#define TIMEMMAFT           1025
//#define TIMEMMAFT           2000
//#define TIMEMMAFT           6025   //MM Long pause not currently implemented

//Number of packets when NEWCMS
#define NFXPAIRS                6
#define NSPEEDPAIRS             8
#define POSNEWTYPE              5
#define POSNEWFGROUP            4    

//Definition of command and type
#include "commands.h"

char bytesMM[2]; //In principle 9 trits=18bits...2bytes (2 extra bits cominf directly from F0.

char locoMM(void);
//char accMM(void);
char getMMaddress(void);
char getMMspeed(void);
char getMMFx(void);
//char getMMacc(char startbyte);
//void idlePacket(void);
void sendMMbyte(char byte);
void sendMMF0();
void Bit0MM(void);
void Bit1MM(void);

#ifdef	__cplusplus
}
#endif

#endif	/* MM_H */

