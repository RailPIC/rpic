/*
 * File:   user.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 26 de febrero de 2013, 9:51
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * User definition of PIC configuration
 * 
 */

/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/


#define PinSignalR            RA5
//#define PinSignalOUT          TRISA5=0
//#define PinSignalZ            TRISA5=1

#define PinSignalB            RA4
#define PinReset              RA3
//#define PinProgOUT            TRISA2=0
//#define PinProgZ              TRISA2=1
#define PinRDCL               RA2
#define PinProgPICclk         RA1
#define PinProgPICdat         RA0

//#define TRISAconf             0b00101111  //Everything high impedance expect RA4 .. actually I think R0, RA1 and RA3 does not matter (old: Ra5, RA2 Hi Impedance Z)
#define TRISAconf             0b00111111  //Everything high .. actually I think R0, RA1 and RA3 does not matter (old: Ra5, RA2 Hi Impedance Z)
//In iddle... Singal Pines are Z... 
#define TRISAout              0b00001111  //When Out Pinsignals are out


#define PinRx                 RC5
#define PinTx                 RC4
#define PinSCOUTin            RC3
#define PinRDDA               RC2
#define PinQUAL               RC1
//#define PinCONSUM             RC0
#define PinCUTOUT             RC0


//#define TRISCconf             0b00110000  //RC5 and RC4 input (RX and TX.. Uart will manage as output when needed) 
//#define TRISCconf             0b00110011  //RC5 and RC4 input (RX and TX.. Uart will manage as output when needed) RC1 and RC0 input too
//#define TRISCconf             0b00110001  //RC5 and RC4 input (RX and TX.. Uart will manage as output when needed)  RC0 input too
//#define TRISCconf             0b00111111  //RC5 and RC4 input (RX and TX.. Uart will manage as output when needed)  All inputs
#define TRISCconf             0b00111110    //RC5 and RC4 input (RX and TX.. Uart will manage as output when needed)  RC0 only output

//POR AQUI

#define ANSELAconf            0b00000000
#define ANSELCconf            0b00000000
//For PIC16f1825 ALTERNATE PIN FUNCTION
#define APFCON0conf           0b00000000  //For RX and TX as dafault

//For PIC16f18326 ALTERNATE PIN FUNCTION
#define RXPPSconf              0b00010101    //RX on RC5  
#define TXPPSconf              0b00010100    //TX on RC4  //not clear if needed
#define RC4PPSconf             0b00010100    //TX on RC4

#define PMD0conf               0b00000001    //Disable IOC
#define PMD1conf               0b01111110    //All timers disabled except TMR0 (so far))
#define PMD2conf               0b11111111    //All analog modueles disabled
#define PMD3conf               0b11111111    //All PWM and CCCP disabled
#define PMD4conf               0b00000111    //USART enabled
#define PMD5conf               0b11111111    //All CLC disabled


//coming from manual and http://www.microchip.com/forums/m701812-print.aspx

#define testbit(var, bit)   ((var) & (1 <<(bit)))
#define setbit(var, bit)    ((var) |= (1 << (bit)))
#define clrbit(var, bit)    ((var) &= ~(1 << (bit)))
#define bittgl(var,bit)     ((var) ^= (1<<(bit)))

#define byte_of(your_var,byte_num) (*(((unsigned char*)&your_var)+byte_num))

/*According to this, I am not applying the bytes right (?)
 
 unsigned long mylong = 0xaAABBCCDD;
 unsigned int newint = 0xEEFF;
 
 #define byte0 byte_of(mylong,0)		// first byte of mylong
 #define byte1 byte_of(mylong,1)		// second byte of mylong
 #define byte2 byte_of(mylong,2)		// third byte of mylong
 #define byte3 byte_of(mylong,3)		// fourth byte of mylong
 
 #define new_lo byte_of(newint,0)	// first byte of newint
 #define new_hi byte_of(newint,1)	// second byte of newint
 
 void main(void)
 {
 
 	PORTA = byte0;		// 0xAA assigned to PORTA
 	PORTB = byte1;		// 0xBB assigned to PORTB
 	PORTA = byte2;		// 0xCC assigned to PORTA
 	PORTB = byte3;		// 0xDD assigned to PORTB
 	byte0 = new_hi;		// tranfer MS byte from newint to LS byte of mylong
 	byte1 = new_lo;
 	
 	...........
 }
 
 */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

void InitApp(void);         /* I/O and Peripheral Initialization */
void InitUSART(void);
void InitTIMER(void);
void InitInt(void);         /*Init interruptions*/
void InitPort(void);        /*Initi port*/
void InitVar(void);         /*Initi port*/
//void InitTrack(void);
//void IddleTrack(void);
void SignalTrack0(void);
void SignalTrack1(void);
void SignalTrackSW(void);
