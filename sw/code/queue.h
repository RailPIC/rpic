/* 
 * File:   queue.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 28 de febrero de 2013, 18:08
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Queue Definitions
 */

#ifndef QUEUE_H
#define	QUEUE_H

#ifdef	__cplusplus
extern "C" {
#endif

#define QUEUESIZE 800
#define EMPTYQUEUE QUEUESIZE+1   //this will also indicate an error if it is return from a find
#define NQUEUES 3
#define QUEUE_NMRA 0
#define QUEUE_MM   1
#define QUEUE_MMX  2

#define MESSAGETYPE1SIZE 5
#define MESSAGETYPE2SIZE 6
#define MESSAGETYPE3SIZE 7
#define MESSAGETYPE4SIZE 11
#define MESSAGETYPE5SIZE 12
#define MESSAGETYPE6SIZE 11
    
//Queue vieja..89% datos; 97% Memoria
//I need to know exactly - and prealocate- the size of the queue... In PIC RAM needs to be defined

//Definition of command and type
#include "commands.h"

typedef struct {

    unsigned int  nextFree;
    unsigned int  position[NQUEUES];
    char positionMMFgroup;                  //F group changes from 0-4 in a rotation for MM Fx packages
    unsigned int  init[NQUEUES];
    char bytes[QUEUESIZE];
    
} queue;

queue locosMessageQueue;

void initQueue(void);
int findatQueue(char subQueue);
char getlocoMessageLength(int position);
int getlocoMessageNext(int position);
int getlocoMessageAddress(int position);
char updateQueue(int position);
void getfromQueue(int position);
void getfromQueueMMXUID(int position);

void initMMXQueue();
int findMMXUID();

//char getMMoldDspeed(void);
//char getMMoldFx(void);
int getMMoldData(void);

#ifdef	__cplusplus
}
#endif

#endif	/* QUEUE_H */


