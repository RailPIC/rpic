/*
 * File:   main.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 12 de marzo de 2013, 10:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Main Routine
 *
 */

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
#include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
#include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
#include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)
#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */
#include "nmra.h"          //NMRA
#include "mm.h"            //MM defintiions
#include "mmx.h"           //MMX defintiions
#include "queue.h"         //Queue is here
#include "serial.h"        //Buffer is here and also abortsignal, status chars
#include "timer.h"         //timer is here
#include "program.h"       //program definitions
#include "test.h"          //test routines
#include "syst_config.h"   //system and Configuration routines

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

char buffer[MAXBYTES]; //defined in serial.h
bit abortsignal; //defined in serial.h
char status; //defined in serial.h
char old_status; //defined in serial.h
bit oldPinSignalR;
bit oldPinSignalB;

/******************************************************************************/
/* Main Program                                                               */

/******************************************************************************/

void main(void) {
    char result;
    char command;
    char i;
    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    InitApp();

    while (1) {

        while (status == TESTON) {
            test();
        }

        while (status == CYCLEON) {
          
            if (locosMessageQueue.position[QUEUE_MMX]!=EMPTYQUEUE){
                //If queue is not init.. at beginning (for MFX) it will never init... because bakes are never out.. and locos will not responde correctly to discover
                //spcial procedure when receving a discover message is needed
                //InitTrack();
                
                getfromQueue(locosMessageQueue.position[QUEUE_MMX]);
                getfromQueueMMXUID(locosMessageQueue.position[QUEUE_MMX]);
                zentral_bake(0);
                crc8(0, BAKE_LENGTH);
                char endping=ping_verify(BAKE_LENGTH+ CRC8_LENGTH);
                crc8(BAKE_LENGTH+ CRC8_LENGTH, endping);
                sendMMXCommand(0, BAKE_LENGTH + CRC8_LENGTH);
                sendMMXCommand(BAKE_LENGTH + CRC8_LENGTH, endping+CRC8_LENGTH);
                SimpleResponseMMX();
                //IddleTrack();
                __delay_us(TIMEBTCYCLE); //For instance.. a delay not necesarilly linked to the 6400us required by resposne
            }
            
            if (locosMessageQueue.position[QUEUE_NMRA]!=EMPTYQUEUE){

                getfromQueue(locosMessageQueue.position[QUEUE_NMRA]);
                result = locoNMRA(); //not error handling implemented yet.. error messages only sent after reciving byte...with checksum
                //errors at this stage only responds to a corrupted queue... only good packets are stored.
                //if queue is empty.. address=0 type=EMPTY...and therefore the result=error10.. we do nto want to transmit such packet... this is treated within the loco()
                //Any other error is only possible with a corrupted queue
                //Some kind of visual feedback with a led could be implemented=iddle mode or corrupted queue
                //PinCUTOUT=1;
                __delay_ms(TIMEBTCYCLE);
                //PinCUTOUT=0;
            }
            
            //InitTrack();
            //Ideally this should be something to select iddle or broadcast for pincutout
            //idlePacket(NORMALpreambleBitsNMRA);
            broadcastPacket(0);
 
            //IddleTrack();     //So far only CUTOUT after iddle package
            PinCUTOUT=1;
            oldPinSignalB=PinSignalB;
            oldPinSignalR=PinSignalR;
            PinSignalR=0;
            PinSignalB=0;
            //__delay_ms(TIMEBTCYCLE);
            __delay_us(464); //Nomina cutout min 454 max 488
            PinSignalR=oldPinSignalR;
            PinSignalB=oldPinSignalB;
            PinCUTOUT=0;
            __delay_us(536); //To complete the 1ms TIMEBTCYCLE
            //Put as a CTE TIMECUTOUT and calculate TIMEBTCYLE*1000-TIMECUTOUT)
            
            if (locosMessageQueue.position[QUEUE_MM]!=EMPTYQUEUE){

                getfromQueue(locosMessageQueue.position[QUEUE_MM]);
                buffer[POSNEWTYPE]=buffer[0];//New place to look for type of buffer
                //In queue only MMn is stroed.. not auxiliarry
                result = locoMM(); //not error handling implemented yet.. error messages only sent after reciving byte...with checksum
                //errors at this stage only responds to a corrupted queue... only good packets are stored.
                //if queue is empty.. address=0 type=EMPTY...and therefore the result=error10.. we do nto want to transmit such packet... this is treated within the loco()
                //Any other error is only possible with a corrupted queue
                //Some kind of visual feedback with a led could be implemented=iddle mode or corrupted queue
                if (buffer[POSNEWTYPE]>LOCOMM1) { //I hav enot implemented LOCOMM1FX... so I can not send LOCOMM1F messages
                    __delay_ms(TIMEBTCYCLE);
                    buffer[POSNEWTYPE]++; //Send a new message but MMnF
                    buffer[POSNEWFGROUP]=locosMessageQueue.positionMMFgroup;
                    result = locoMM(); 
                }
                __delay_ms(TIMEBTCYCLE);
            //TRISA = TRISAout;
            }

  
            
            
            if (locosMessageQueue.position[QUEUE_MMX]!=EMPTYQUEUE){
                //If queue is not init.. at beginning (for MFX) it will never init... because bakes are never out.. and locos will not responde correctly to discover
                //spcial procedure when receving a discover message is needed
               //  InitTrack();
            
                getfromQueue(locosMessageQueue.position[QUEUE_MMX]);
                //getfromQueueMMXUID(locosMessageQueue.position[QUEUE_MMX]);
                char endloco=locoMMX(0);
                crc8(0, endloco);
                sendMMXCommand(0, endloco + CRC8_LENGTH);
                EndFlag();
                //PinSignalZ;
                //IddleTrack();
                __delay_us(TIMEBTCYCLE); //For instance.. a delay not necesarilly linked to the 6400us required by resposne
            }
            
           

            if (!abortsignal) { //if abortsingal.. we do not increase and repeat packet
                //if not abortsingal.. increment queue.. otherwise we will continue with the old count
                //potential problem if interruption happens here... but at this stage last packet has been sent..so no problem in updating
                if (locosMessageQueue.position[QUEUE_NMRA]!=EMPTYQUEUE){
                    locosMessageQueue.position[QUEUE_NMRA]=getlocoMessageNext(locosMessageQueue.position[QUEUE_NMRA]);
                }
                if (locosMessageQueue.position[QUEUE_MMX]!=EMPTYQUEUE){
                    locosMessageQueue.position[QUEUE_MMX]=getlocoMessageNext(locosMessageQueue.position[QUEUE_MMX]);
                }
                if (locosMessageQueue.position[QUEUE_MM]!=EMPTYQUEUE){
                    locosMessageQueue.position[QUEUE_MM]=getlocoMessageNext(locosMessageQueue.position[QUEUE_MM]);
                    locosMessageQueue.positionMMFgroup++;
                    if (locosMessageQueue.positionMMFgroup>4) locosMessageQueue.positionMMFgroup=1;
                }
            }

            if (bytesreceived == 0) //if timeout or free to receive new message
            { //if a new message has arrived.. abortsignal is false but status is newcms
                abortsignal = false; //if interruption right here...is newmessage... abortinsingal==true (as expected.. new messages break the reception
            } //always free at some point the signal.. otherwise it can hang after an interruption and never start again.
        }

        //i do not di() before checking status to avoid di() when cycle off..otherwise.. never come back from cycleoff
        if (status == NEWCMD) { //if interruption happens just here (new receptioN).. abortinsingal=true. I changege the aborting signal afert di().. New message will be lost since timeout will happen. timeout and sender will send it again
            //di(); //no interruptions when treating new commands       //so far enable rest of interruptions.. in case i want another peripherial to interrupt
            RCIE = 0; //no new meesages allowed when reading.. I need interruption for the timers in the signal.. not anymore
            abortsignal = false; //in case an interruption arrives just at the beginning if (staus==NEWCMS)
            command = (bufferIN[0]) &0b00001111;
            //for (i = 0; i != MAXBYTES; i++) {
            for (i = 1; i != MAXBYTES; i++) {
                buffer[i - 1] = bufferIN[i]; //I need two buffers.. whilest preparing bufferIN if we are in the loop get from queue changes buffer... so I need to secure buffeerIN
                //buffer ([0])=bufferIN[1]=type
                //Easier to reduce in one byte the bufferIN.. all code was built when buffer[0] was command and type.. and not command and length]
            }
            buffer[MAXBYTES - 1] = 0; //to stro the nlenght of the response... if any different to checksum ACk

            if (command == SYSTEMCMD) { //SWITCH case not supported by PIC16..takes a long block
                if (buffer[0] != ERROR1) {
                    result = systemCMD();
                } else {
                    result = ERROR1;
                }
            }
            if (command == CONFIGCMD) {
                result = configCMD();
            }
            if (command == ACCNMRACMD) {
                result = accNMRA();
            }
            if (command == LOCNMRACMD) {
                result = locoNMRA();
            }
            if (command == LOCMMCMD) {
                buffer[POSNEWTYPE]=buffer[0];
                //oldDspeed = getMMoldDspeed();
                int oldData=getMMoldData();
                char oldSpeed=byte_of(oldData, 0);
                char oldFx=byte_of(oldData, 1);
                if ((buffer[POSNEWTYPE] == LOCOMM1)&((oldSpeed^buffer[2]) > 127)) { //oldspeed xor current speed... if change in bit 7.. xor makes b7=1...so
                    buffer[POSNEWTYPE] = LOCOMM1R;
                }
                if ((buffer[POSNEWTYPE] == LOCOMM2)&(oldSpeed == buffer[2])) {
                    //No changes in speed.. let's see what function has changed
                    char fchange = buffer[3]^oldFx; //If F0 changed=1... Fgroup=0; If F1 changed=10=2 Fgroup=1...if f2 changed=100=4 fgroup=2... f3=1000=8
                    buffer[POSNEWTYPE] = LOCOMM2Fx;
                    switch (fchange) {
                        case 0: //Same speed but no change (first command sent is  Fx=0)
                        case 1:
                            buffer[POSNEWTYPE] = LOCOMM2; //By default... change f0.. treated as speed command
                            break;
                        case 2:
                            buffer[4] = 1;
                            break;
                        case 4:
                            buffer[4] = 2;
                            break;
                        case 8:
                            buffer[4] = 3;
                            break;
                        case 16:
                            buffer[4] = 4;
                            break;
                    }
                }
                result = locoMM();
            }
            if (command==LOCMMXCMD){         
                //InitTrack();
            
                char endloco=locoMMX(0);
                crc8(0, endloco);
                sendMMXCommand(0, endloco + CRC8_LENGTH);
                EndFlag();
                //IddleTrack();
            
            }
            if (command == PROGCMD) {
                result = program();
            }
            status = old_status;
            WriteUSART(buffer[MAXBYTES - 2] + result); //in bufferIN[MAZBYTES-1]=last position I am stroing checksum... when moving bufferIN to tbuffer... position is decreased
            //in buffer[MAXBYTES-1] I could store length of message to be returned.
            for (i = 0; i < buffer[MAXBYTES - 1]; i++) { //Send a�� bytes of response (also in buffer)
                WriteUSART(buffer[i]);
                //for (i=0;i<11;i++){
                //    WriteUSART(i);
            }
            //WriteUSART(buffer[MAXBYTES - 1] + result);
            //So far no other status defined.
            //ei();
            commandlength = 0; //ready for new packet
            RCIE = 1; // new meesages allowed when reading.. I need interruption for the timers in the signal
        }
    }
}
