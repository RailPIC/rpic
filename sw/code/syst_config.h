/*
 * File:   syst_config.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 3 de marzo de 2016, 18:48
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * System and configuration routines
 * 
 */
#include <xc.h>

#define VERSIONH 0x01
#define VERSIONL 0x19

//Curently hard coded in mmx.h
#define AZENTRAL  0x00 //Addreess EEPROM ZENTRAL
#define ANSESSION 0x04 //Addreess EEPROM NSESSION

//New MS=0x2B5BD?  //N session=6;
extern char ZENTRAL[4];
extern char NSESSION[2];

#ifndef SYST_CONFIG_H
#define	SYST_CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* SYST_CONFIG_H */


char systemCMD();
char configCMD();
