/* 
 * File:   mmx.h
 * Author: Manolo
 * http://tren.enmicasa.net
 *
 * Created on 20 de marzo de 2013, 22:11
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  MM Definitions
 *
 * Timing adapted for the current routines and 32Mhz clock
 * Further timing improvements may lead to new timing definitions
 * Faster cloks also requires new timing definitions
 *
 * Standar timing should be:
 * SHORT: 50us
 * LONG: 100us
 * 
 */

#ifndef MMX_H
#define	MMX_H

#ifdef	__cplusplus
extern "C" {
#endif

#define TIME1MMX   50
#define TIME0MMX   100

#define TIME1MMXL  36
#define TIME1MMXH  50
#define TIME0MMXL  85
#define TIME0MMXH  85
 
//#define MAXBYTESMMX 9
#define MAXBYTESMMX 25
    
#define TIMERESPONSE 6400
#define TIMERESPONSE_100 63  //To be adjusted empirically
#define POSITIVE_LIMIT   150 //To avoid aritmetic average calculation (division)
#define NEGATIVE_LIMIT   100

#define BAKE_LENGTH          63
#define DISCOVERY_LENGTH     53
#define BIND_LENGTH          61
#define CRC8_LENGTH          8
#define UID_LENGTH           32
    
    
//Definition of command and type
#include "commands.h"

//#define ZENTRAL 0x0002045F; 0x2045F; New MS=0x2B5BD?
#define ZENTRAL0  0xBD
#define ZENTRAL1  0xB5
#define ZENTRAL2  0x02
#define ZENTRAL3  0x00 
#define NSESSION0 0x06   //Nsession=6 
#define NSESSION1 0x00 
    
  
    
char byteMMX[MAXBYTESMMX];         //25*8=200 bits... so far it is enough..MORE TTHAN 256 WPULD REQUIRE CHANGING ALL FUNCTIONS

//global bit position= bytenumber*8+bitnumber;

char locoMMX(char start);
void zentral_bake(char start);
void lok_discovery(char start);
void bind_nadr(char start);
char ping_verify(char start);

void broadcastAddress(char globalbit);
char mfxAddress(char globalbit);
char mfxSpeed(char globalbit);
char mfxFunction(char globalbit);
void deco_uid(char globalbit);
void EndFlag();
void SimpleResponseMMX();
void FlagMMX(void);

//void crc8(char length);
void crc8(char start, char end);
void Bit1MMX(void);
void Bit0MMX(void);

void putbitMMX(char globalbit,char b);    //it is not possible to pass a bit as parameter
bit getbitMMX(char globalbit);
//void sendMMXCommand(char length);
void sendMMXCommand(char start,char end); //to be able to send various linked commands

#ifdef	__cplusplus
}
#endif

#endif	/* MMX_H */
