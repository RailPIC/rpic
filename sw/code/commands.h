/* 
 * File:   commands.h
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 6 de abril de 2013, 11:42
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This file contains the definitions of the commands and type properties of the messages betweern ROCRRAIL adn ROCPI
 * It also includes the ERROR and STATES definitions
 */

#ifndef COMMANDS_H
#define	COMMANDS_H

#ifdef	__cplusplus
extern "C" {
#endif

//Command field within the command/type byte
#define SYSTEMCMD      0
#define ACCNMRACMD     1
#define ACCMMCMD       2
#define LOCMMCMD       3
#define LOCMMXCMD      4
#define LOCNMRACMD     5
#define PROGCMD        6
#define CONFIGCMD      7

//STATES 
//Cycle off and on defined afterwards
//#define CYCLEOFF        0       //State and command. POWER ON RAIL
//#define CYCLEON        250       //State and command. POWER OFF RAIL
#define NEWCMD         2       //State, new command received
//Test ON defined after
//#define TESTON          50

    //Resposnse ERRORS
#define OK             0       //Response: OK
#define ERROR1         1       //Checksum error    MAIN ERROR coded also as a type of SYSTEMCMS
#define ERROR10        10      //Address error.. A number that is different to the normal outputs of get_address functions
#define ERROR20        20      //CMD not available
#define ERROR30        30      //QUEUE complet


    //#define ERROR50000     50000   //QUEUE mismatch... ERRO number must be bigger than queue size 
    //Type of SYSTEMCMD 
#define CYCLEOFF        0       //State and command. POWER OFFN RAIL
#define CYCLEON        250      //State and command. POWER ON RAIL
#define OVERLOAD       50		//OVERLOAD SHORT CIRCUIY. Not to be sent from RPIC, but from rpicd to stations. It could be sent to launch a cycleOFF (keep a status until it is clear
#define STATUSc         51		//STATUS consumtion. Not to be sent from RPIC, but from rpicd to stations
#define PTON           100      //Command. Programming track ON (clock on PT and all CMD to PT and not in queue) PT use to programm or as test locos
#define PTREADY_MM	   101		//Get PT ready for a command. set speeds to 0 anf fn=0 going through MM queue and sends a single command (PTON) coming back to old PT status (PTON or PTOFF)
#define PTREADY_NMRA   102		//Get PT ready for a command. set speeds to 0 anf fn=0 going through NMRA queue and sends a single command (PTON) coming back to old PT status (PTON or PTOFF)
#define PTREADY_MMX    103		//Get PT ready for a command. set speeds to 0 anf fn=0 going through MMX queue and sends a single command (PTON) coming back to old PT status (PTON or PTOFF)
#define PTREADY_ACC_MM 104		//Get PT ready fir a command. Set all outputs off
#define PTREADY_ACC_NMRA  105	//Get PT ready fir a command. Set all outputs off
#define SYSTEM_HALT       120	//All locos to zero speed, keeping power. No clear if it is an emergency break, or just speed=0
#define EMERGENCY_BREAK   121	//Emergency break per loco.
#define REMOVE_FROM_CYCLE 122	//Remove loco from cycle
#define PING_SW           123	//Not to be sent to rpic. To response to a some SW with specific details on mcs2
#define RESETSTATION      125	//Reset command
#define S88_POLLING       140	//Not to be sent to rpic, to recevie and send repsone to stations
#define S88_EVENT         141	//Not to be sent to rpic, to be sent to stations
#define PTOFF             200   //Command. Programming track OFF
//#define ERROR1         1      //Checksum error    MAIN ERROR coded also as a type of SYSTEMCMS
    //Other commands to be treated carefully. By default a command is also a state, if this is not the case, it has to be coded on the command triage
#define PING           150       //Command. Ping UID
#define DISCOVERY      151       //Command. Lok-Discovery
#define BIND           152       //Command. Bind new SID Address
#define INIT_MMX       153       //Init MMX Queue
#define FAST_MMX_READ  154	     //Fast read of MMX decoders

//Type of CONFIGCMD
#define TESTON          50		//Activate Test cycle (not for operation). test code to be included and compile eahc time
#define TESTOFF         200		//Deactivate Test cycle
#define VERSION_READ    210     //State and command. POWER ON RAIL
#define ZENTRAL_WRITE   150       //Command. Send Zentral UID
#define ZENTRAL_READ    151       //Command. Send Zentral UID
#define SESSION_N_WRITE 152		//Stablish new session Nr.
#define SESSION_N_READ  153		//Stablish new session Nr.
#define ENABLEPROT      10		//Enable prot (to rpic and rpicd) (MM, NMRA, MMX)
#define CONF_DEVICES    12		//Conf devices. Not to be send to rpic. It seems from rpicd to other devices (like swithces or S88 modules)
#define CONFIG_STATUS   14		//Not to be sent to rpic. Configure the way status is reported
#define REQUESTCONF     15		//Not to be sent to rpic. Request configuration
#define CONFIG_DATA     16		//Not to be sent to rpic. Not clear how to use it
#define CONFIG_AUT      17		//Not to be sent to rpic. Not clear how to use it
#define CONFIG_BOOT     18		//Not to be sent to rpic. Not clear how to use it
#define CHANGE_LOC_TYPE 19	//Change type of loco (already in cycle)... to avoid loco in cycle keeping reciving messages
#define SW_TIME         20	//Switch time. Not in use so far since it is not possible to distinguish outputs from switches

//Type field within the command/type byte
//for LOCMMCMD and ACCMMCMD
#define ACCMM                0  //Not implemented so far
#define LOCOMM1              1
#define LOCOMM1Fx            2  //Not implemented so far
#define LOCOMM1R             3  //virtual type to note internally a change in direction. Not to be stored
#define LOCOMM2              4
#define LOCOMM2Fx            5  //virtual type to store functions
#define LOCOMM3              6  //Not implemented so far
#define LOCOMM3Fx            7  //Not implemented so far
#define LOCOMM4              8  //Not implemented so far
#define LOCOMM4Fx            9  //Not implemented so far
#define LOCOMM5             10  //Not implemented so far
#define LOCOMM5Fx           11  //Not implemented so far

//for  LOCMMXCMD
//alpha version, disable form current version
#define LOCOMMX_F4         200
#define LOCOMMX_F8         201
#define LOCOMMX_F16        202
#define LOCOMMX_FN         203

//for LOCNMRACMD and ACCNMRACMD
//NMRA -LOCOXXX must be numbers different to the types used for MM and MMX and bigger
#define ACCNMRA              0
#define LOCODCCN14           100
#define LOCODCCN28           101
#define LOCODCCN128          102
#define LOCODCCNfx           103
#define LOCODCCL14           104
#define LOCODCCL28           105
#define LOCODCCL128          106
#define LOCODCCLfx           107
//NULL definition
#define EMPTY                255  //used to init queue

//Type field within the command/type byte
//for PROGCMD
#define NMRA_SERVICE_MODE_ADDRESS_write     0
#define NMRA_SERVICE_MODE_ADDRESS_read      1
#define NMRA_SERVICE_MODE_DIRECT_bit        2
#define NMRA_SERVICE_MODE_REGISTER_write    3
#define NMRA_SERVICE_MODE_REGISTER_read     4
#define NMRA_SERVICE_MODE_PAGE_write        5
#define NMRA_SERVICE_MODE_PAGE_read         6
#define NMRA_SERVICE_MODE_DIRECT_write      7
#define NMRA_SERVICE_MODE_DIRECT_verify     8
#define ACCNMRAwrite                        50
#define ACCNMRAverify                       51
#define ACCNMRAbit                          52
#define LOCODCCNwrite                       53
#define LOCODCCNverify                      54
#define LOCODCCNbit                         55
#define LOCODCCLwrite                       56
#define LOCODCCLverify                      57
#define LOCODCCLbit                         58
#define MM                                  100
#define MMX                                 150

#ifdef	__cplusplus
}
#endif

#endif	/* COMMANDS_H */
