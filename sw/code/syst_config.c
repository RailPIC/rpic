/*
 * File:   syst_config.c
 * Author: Manolo Serrano
 * http://tren.enmicasa.net
 *
 * Created on 3 de marzo de 2013, 18:48
 *
 *  This file is part of rpic.
 *
 *  rpic is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  rpic is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rpic.  If not, see <http://www.gnu.org/licenses/>.
 *
 * System and configuration routines
 * 
 */
#include "syst_config.h"
#include "user.h"
#include "serial.h"
#include "commands.h"
#include "mmx.h"
#include "program.h"
#include "queue.h"

char ZENTRAL[4];
char NSESSION[2];
//Init EEPROM
//Not working well
__EEPROM_DATA(0x00,0x02,0xB5,0xBD,0x00,0x06,0x00,0x00);

char systemCMD(){
    char length_ping;
    switch (buffer[0]) {
        case PING:
            //InitTrack();
            
            length_ping=ping_verify(0);
            crc8(0,length_ping);
            sendMMXCommand(0,length_ping+CRC8_LENGTH);
            SimpleResponseMMX();
           // IddleTrack();
            break;
        case DISCOVERY:
         //   InitTrack();
            lok_discovery(0);
            crc8(0,DISCOVERY_LENGTH);
            sendMMXCommand(0,DISCOVERY_LENGTH+CRC8_LENGTH);
            SimpleResponseMMX();
        //    IddleTrack();
             break;
        case BIND:
          //  InitTrack();
            bind_nadr(0);
            crc8(0,BIND_LENGTH);
            sendMMXCommand(0,BIND_LENGTH+CRC8_LENGTH);
            EndFlag();
        //    IddleTrack();
            
            //Fala hacer un update.. pero busca por direcci�n.. no por UID... habria que borrar la vieja y hacer un init de la nueva!!!
            //tampoco funcionaria pq bind no lleva infor de type o nf... hay que hacer busqueda por UID
            
            //Not sure if needed a delay here
            break;
        case INIT_MMX:
            initMMXQueue();
            break;
        case PTON:
            //PTactive=1;
            break;
        case PTOFF:
            //PTactive=0;
            break;
/*        case CYCLEON:
            old_status = buffer[0];
            InitTrack();
            TRISA =TRISAout;
            break;
        case CYCLEOFF:
            old_status = buffer[0]; //can be cycleON or cycleOFF.. i stored in old_status becuaseit is going to be recovered later
            //IddleTrack(); //at start cycle on will power init track again
            PORTA=0;
            TRISA =TRISAconf;
            break;
 * */
        case CYCLEON:
        case CYCLEOFF:
            old_status = buffer[0]; //can be cycleON or cycleOFF.. i stored in old_status becuaseit is going to be recovered later
            PinCUTOUT=1;
            break;
        default:
            return ERROR20;
    }
    //In principle in rocpi the status of PT does not affect the cycle... independant pins... in principle.. let's see how it evolves in the HW

    //So far no ERROR treatment from any SystemCMD    
    return OK;
}
char configCMD(){

    switch (buffer[0]) {
        case TESTOFF:
            old_status = CYCLEON;   //For testing
            break;
        case TESTON:
            old_status = buffer[0]; //can be TESTON
            break;
        case VERSION_READ:
            buffer[MAXBYTES - 1]=2;
            buffer[0]=VERSIONH;
            buffer[1]=VERSIONL;
            break;
        case ZENTRAL_READ:
            buffer[MAXBYTES - 1]=4;
            for (char i=0;i<4;i++) buffer[i]=ZENTRAL[i];
            break;
        case ZENTRAL_WRITE:
            for (char i=0;i<4;i++){
                ZENTRAL[i]=buffer[1+i];
                eeprom_write(AZENTRAL+i,ZENTRAL[i]);
            }
            break;
        case SESSION_N_WRITE:
            for (char i=0;i<2;i++){
                NSESSION[i]=buffer[1+i];
                eeprom_write(ANSESSION+i,NSESSION[i]);
            }
            break;
        case SESSION_N_READ:
            buffer[MAXBYTES - 1]=2;
            for (char i=0;i<2;i++) buffer[i]=NSESSION[i];
            break;
        default:
            return ERROR20;
    }                
    return OK;
}