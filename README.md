# RAILPIC PROJECT #

|                       |                                                      |
|:----------------------|:-----------------------------------------------------|
|**Project Owner(s):**  |Manuel Serrano                                        |
|**Author(s):**         |Manuel Serrano                                        |
|**Hardware Platform:** |ODROID, RASPBERRY PI, PIC 16f18326, PIC16f1825        |
|**Debuggers Used:**    |GCC, ECLIPSE, MPLAB Simulator                         |
|**Programmers Used:**  |PICKIT3, PICKIT2                                      |
|**MPLAB Version:**     |>1.70                                                 |
|**C Compiler Version:**|1.12<xc8<2.0 gcc>4.8.4                                |

**For more information:** [RailPic Homepage](http://tren.enmicasa.net/railpic)  

********************************************************************************

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

********************************************************************************

RAILPIC is a project to embed the control of model railways in a PIC ucontroller
connected to an arm device..

Currently the project provides:


*   Support for MM1 and MM2 protocol
*	Support for NMRA (long and short addresses) – 14,28 and 128 steps
*	Support for MMX (MM4) (not yet reading responses, but with the UID of 
the loco, it is possible to register the loco and send commands to it.)
*	Support for NMRA accessories (no extended accessories so far)
*	Support for writing CV.

The definition of the MM protocol was obtained from:

- ["The Manual of the new Märklin Motorola Format"](http://spazioinwind.libero.it/scorzoni/motorola.htm), 
Andrea Scorzoni, 1995-2000.  

The definition of the MFX protocol was obtained from:
- ["Das mfx-Schienenformat"](http://www.skrauss.de/modellbahn/mdigital.html),
Stefan Krauß, Version 1 2009-02-01.  
- [Rainer Mueller web site](http://www.alice-dsl.net/mue473/mfxrahmen.html).

## FOLDERS INCLUDED IN THE PROJECT TREE ##

- sw, the code and the releases of the firmware.  
- hw, the 2 versions of the rpic board, with schematics and gerber files.  
- LVP, a Low Voltage Programming utility to program the board from an ARM device.